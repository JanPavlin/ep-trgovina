<?php
require_once '../../resources/database/database.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class AuthController{
    public static function indexPageSeller(){  
        $rules = [
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range'=>5]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        //var_dump($data);
        if(isset($data)){

            echo ViewHelper::render("../../view/seller/sellerLogin.php", [
                "error" => $data['error']
            ]);
        }else{
            throw new InvalidArgumentException();
        }
        
    }
    public static function indexPageAdmin(){
        //$rs = json_decode(file_get_contents("http://10.0.2.2/netbeans/rest-api/books"), TRUE);
        //var_dump($rs);
        echo ViewHelper::render("../../view/admin/adminLogin.php");
    }
    public static function indexPageClient(){
        $rules = [
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range'=>5]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        echo ViewHelper::render("../../view/accounts/login.php", [
            "error" => $data['error']
        ]);
    }
    public static function indexPageClientRegister(){
        $rules = [
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range'=>7]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        echo ViewHelper::render("../../view/accounts/register.php", [
            "error" => $data['error']
        ]);
    }
    public static function  sellerLogin(){
        $rules = [
            "username" => FILTER_VALIDATE_EMAIL,
            "password" => FILTER_SANITIZE_SPECIAL_CHARS
        ];
        
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            $arr = DB::login($data['username']);
            if ($arr){

                if(sha1($data['password'])== $arr['geslo'] && $arr['id_statusosebe'] == '2'){
                    sellerSession::createSellerSession($arr['id_oseba']);
                    ViewHelper::redirect(BASE_URL . "sellerPage"."?error=0");

                }else{
                    ViewHelper::redirect(BASE_URL . "?error=1");
                }
            }
            else {
                ViewHelper::redirect(BASE_URL . "?error=1");
            }
            
        }
        
    }
    
    public static function adminLogin(){
        $data = filter_input_array(INPUT_POST, self::getRulesLogin());
        if(self::checkValues($data)){
            $userdata = DB::login($data["name"]);
            //preveri, ce se gesli ujemata
            //var_dump($userdata);
            if($userdata["geslo"] == sha1($data["password"])){
                adminSession::createAdminSession($userdata["id_oseba"]);
                ViewHelper::redirect(BASE_URL."adminPage?error=3");
                //var_dump($userdata);
            }else{
                //preusmeritev nazaj na admin login.
                ViewHelper::redirect(BASE_URL);
            }
            
        }else{
            throw new InvalidArgumentException();
        }
    }
    
    public static function clientRegister(){
        var_dump($_POST["g-recaptcha-response"]);
        // your secret key
        $secret = "6LfGdj4UAAAAAJslL7eBnBaXnx-B5XJHinQdBxAJ";

        // empty response
        $response = null;

        // check secret key
        $reCaptcha = new ReCaptcha($secret);
        
        if ($_POST["g-recaptcha-response"]) {
            $response = $reCaptcha->verifyResponse(
                $_SERVER["REMOTE_ADDR"],
                $_POST["g-recaptcha-response"]
            );
        }
        
          if ($response != null && $response->success) {
              $rules = [
                "email" => FILTER_VALIDATE_EMAIL,
                "password" => FILTER_SANITIZE_SPECIAL_CHARS,
                "confirm-password" => FILTER_SANITIZE_SPECIAL_CHARS,
                "customer-first-name" => FILTER_SANITIZE_SPECIAL_CHARS,
                "customer-last-name" => FILTER_SANITIZE_SPECIAL_CHARS,
                "telephone-number" => FILTER_VALIDATE_INT,
                 "kraj" => FILTER_SANITIZE_SPECIAL_CHARS,
                  "ulica" => FILTER_SANITIZE_SPECIAL_CHARS,
                  "stevilka" => FILTER_SANITIZE_SPECIAL_CHARS,
                  "posta" => FILTER_VALIDATE_INT
            ];
            $data = filter_input_array(INPUT_POST, $rules);
           // var_dump(strlen((string)$data['telephone-number']));
            if(self::checkValues($data)){
               if($data['password'] == $data["confirm-password"]){
                   if(strlen((string)$data['telephone-number']) == 9 ){
                      if(DB::registerStranka($data["customer-first-name"], $data["customer-last-name"], $data["email"],
                              $data["password"], $data["telephone-number"], $data['posta'], $data['kraj'], $data['ulica'], $data['stevilka'])){
                           ViewHelper::redirect(BASE_URL."register?error=2");
                       }else{
                            ViewHelper::redirect(BASE_URL."register?error=7");
                       }
                   }else{
                       ViewHelper::redirect(BASE_URL."register?error=3");
                   }   
               }
               else{
                   ViewHelper::redirect(BASE_URL."register?error=1"); //gesli se ne ujemata
               }
                var_dump($data);
            }else{
                ViewHelper::redirect(BASE_URL."register?error=3");
                //throw  new InvalidArgumentException;
            }
          } else {
              ViewHelper::redirect(BASE_URL."register?error=6");
          }
        
        
        
    }
    
    public static function clientLogin(){
           $rules = [
            "username" => FILTER_VALIDATE_EMAIL,
            "password" => FILTER_SANITIZE_SPECIAL_CHARS,
           
        ];
        
        $data = filter_input_array(INPUT_POST, $rules);
        //var_dump($data);
        if(self::checkValues($data)){
            $arr = DB::login($data['username']);
            //var_dump($arr);
            if ($arr){
                //stutus 4 pri uporabniku pomeni, da je ok...
                if(sha1($data['password'])== $arr['geslo'] && $arr['id_statusosebe'] == '4'){
                    clientSession::createClientSession($arr['id_oseba']);
                     ViewHelper::redirect(BASE_URL."clientMain");

                }else{
                    //var_dump("napacni podatki");
                    ViewHelper::redirect(BASE_URL . "login?error=2");
                }
                
            }
            else {
                //var_dump("ni userja");
                ViewHelper::redirect(BASE_URL . "login?error=1");
            }
            
        }else{
            ViewHelper::redirect(BASE_URL . "login?error=3");
        }
    }
    
    public static function clientLogOut(){
        $rules = [
            'logOut_confirm' => FILTER_REQUIRE_SCALAR,
          
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            //unici sejo in preusmeri nazaj..
            clientSession::destroySession();
            Cart::purgeCart(); //ko unicis sejo se spodobi unicit vse kar je tudi v kosarici.
            ViewHelper::redirect(BASE_URL."?error=0");
        }else{
             ViewHelper::redirect(BASE_URL . "clientMain");
        }
    }
    
    public static function deleteAcc(){
        $rules = [
            'id' =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
         $data = filter_input_array(INPUT_GET, $rules);
         if(self::checkValues($data)){
             DB::deleteOsebaId($data['id']);
             clientSession::destroySession();
            Cart::purgeCart(); //ko unicis sejo se spodobi unicit vse kar je tudi v kosarici.
            ViewHelper::redirect(BASE_URL."?error=0");
         }else{
             throw new InvalidArgumentException;
         }
    }
    
    
    ///Helper funkcije....
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }
        
        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
    
    
    private static function getRulesLogin() {
        return [
            'name' => FILTER_VALIDATE_EMAIL,
            'password' => FILTER_SANITIZE_SPECIAL_CHARS,
        ];
    }
    
}