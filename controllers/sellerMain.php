<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SellerMain {
    public static function mainSeller(){
        $rules = [
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range' =>8] //manj kot 0 ne sme bit id se mi zdi
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        if(isset($data)){
                $allProducts = DB::getIzdelekAll();
                $allNew = [];
                foreach($allProducts as $key=> $product){
                    //var_dump($product);
                    $productScoreAll = DB::allOcenaZaProduct($product['id_izdelka']);
                    $score = 0.0;
                    foreach($productScoreAll as $key=>$one){
                        $score += $one['vrednost'];
                    }
                    if(!empty($productScoreAll)){
                        $score /= count($productScoreAll);
                    }
                    $product['score'] = $score;
                    $pictures = DB::getSlika($product['id_izdelka']);
                    if(!empty($pictures)){
                        $product['slika'] = $pictures[0]['potSlike'];
                    }else{
                        $product['slika'] = null;
                    }
                    
                    $allNew[$key] = $product;
                    //var_dump($product);
                }
               // var_dump($allNew);
            
                       
            echo ViewHelper::render("../../view/seller/sellerMainWeb.php", [
            "sellerData" => DB::getSelf($_SESSION['seller'])[0],
            "allProducts" => $allNew,
            "allCustomers" => DB::getAllStranka(),
            "error" => $data['error'],
            "oddanaNarocila" => DB::getNarocilaNeobdelana(),
            "potrjenaNarocila" => DB::getNarocilaPotrjena(),
            "storniranaNarocila" => DB::getNarocilaStornirana()
            ]);
        }else{
            throw new InvalidArgumentException;
        }

    }
    
    public static function logOut(){
         $rules = [
            'logOut_confirm' => FILTER_REQUIRE_SCALAR,
          
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            //unici sejo in preusmeri nazaj..
            sellerSession::destroySession();
            ViewHelper::redirect(BASE_URL."?error=0");
        }else{
             ViewHelper::redirect(BASE_URL . "");
        }
    }
    
    public static function newProductForm(){
        $rules = [
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range'=>5]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        //var_dump($data);
        if(isset($data)){
            echo ViewHelper::render("../../view/seller/newProduct.php", [
                "error" => $data['error']
            ]);
        }
        
    }
    
    public static function newProduct(){
        $rules = [
            "product" => FILTER_SANITIZE_SPECIAL_CHARS,
            "description" => FILTER_SANITIZE_SPECIAL_CHARS,
            "price" =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1, 'max_range'=>100]
            ],
            "status" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1, 'max_range'=>2]
            ]
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        
        if(self::checkValues($data)){
            if($data['product'] == "" || $data['description'] == " "){
                ViewHelper::redirect(BASE_URL . "newProduct?error=1");
            }else{
                DB::addIzdelek($data['product'], $data['status'], $data['description'], $data['price']);
                ViewHelper::redirect(BASE_URL . "newProduct?error=2");
            }
            
        }else{
           throw new InvalidArgumentException();
        }
    }
    
    public static function chnageData(){
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "name" => FILTER_SANITIZE_SPECIAL_CHARS,
            "surname" => FILTER_SANITIZE_SPECIAL_CHARS,
            "username" => FILTER_VALIDATE_EMAIL //filteriranje emaila.
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            if($data['name']=='' || $data['surname']=='' || $data["username"] == ''){
             ViewHelper::redirect(BASE_URL . "sellerPage?error=0");   
            }else{
                $dataOseba = DB::getSelf($data['id'])[0];
                var_dump($data, "klicar se mora funkcija za spremembo...");
                DB::ChangeDataOseba($data['id'], $data['name'], $data['surname'], $dataOseba['id_statusosebe'], $data['username'], $dataOseba['geslo']);
                ViewHelper::redirect(BASE_URL . "sellerPage?error=2");
            }
            
        }else{
             ViewHelper::redirect(BASE_URL . "sellerPage?error=1");  //spremba nemogoca, ker ni sel cez filter
        }
    }

    public static function changePasswordSellerMain(){
         $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "passwordOld" => FILTER_SANITIZE_SPECIAL_CHARS,
            "passwordNew" => FILTER_SANITIZE_SPECIAL_CHARS,
            "passwordConfirm" => FILTER_SANITIZE_SPECIAL_CHARS
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        var_dump($data);
        if(isset($data)){
            //var_dump($user);
            if($data['passwordNew'] == $data['passwordConfirm']){
                //var_dump("vse vredu, potreben je klic da se geslo spremeni");
                DB::changePassword($data['id'], $data['passwordNew']);
                ViewHelper::redirect(BASE_URL . "sellerPage?error=5");
            }
            else{
                ViewHelper::redirect(BASE_URL . "sellerPage?error=4");
            }
            
        }else{
            //var_dump("err");
            //ViewHelper::redirect(BASE_URL . "adminPage");
            throw new InvalidArgumentException();
        }
    }
    
    public static function newClient(){
         $rules = [
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range'=>7]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        echo ViewHelper::render("../../view/seller/newClient.php", [
            "error" => $data['error']
        ]);
    }
    
    public static function newClientRegister(){
        //var_dump($_POST["g-recaptcha-response"]);
        // your secret key
        $secret = "6LfGdj4UAAAAAJslL7eBnBaXnx-B5XJHinQdBxAJ";

        // empty response
        $response = null;

        // check secret key
        $reCaptcha = new ReCaptcha($secret);
        
        if ($_POST["g-recaptcha-response"]) {
            $response = $reCaptcha->verifyResponse(
                $_SERVER["REMOTE_ADDR"],
                $_POST["g-recaptcha-response"]
            );
        }
        
          if ($response != null && $response->success) {
                $rules = [
                "email" => FILTER_VALIDATE_EMAIL,
                "password" => FILTER_SANITIZE_SPECIAL_CHARS,
                "confirm-password" => FILTER_SANITIZE_SPECIAL_CHARS,
                "customer-first-name" => FILTER_SANITIZE_SPECIAL_CHARS,
                "customer-last-name" => FILTER_SANITIZE_SPECIAL_CHARS,
                "telephone-number" => FILTER_VALIDATE_INT,
                "kraj" => FILTER_SANITIZE_SPECIAL_CHARS,
                "ulica" => FILTER_SANITIZE_SPECIAL_CHARS,
                "stevilka" => FILTER_SANITIZE_SPECIAL_CHARS,
                "posta" => FILTER_VALIDATE_INT
            ];
            $data = filter_input_array(INPUT_POST, $rules);
           
           // var_dump(strlen((string)$data['telephone-number']));
            if(self::checkValues($data)){
               if($data['password'] == $data["confirm-password"]){
                   if(strlen((string)$data['telephone-number']) == 9 ){     //TODO : ČE JE PREDOLG STRING/ NAPAČEN FORMAT
                       if(DB::registerStranka($data["customer-first-name"], $data["customer-last-name"], $data["email"],
                               $data["password"], $data["telephone-number"], $data['posta'], $data['kraj'], $data['ulica'], $data['stevilka'])){
                           var_dump("redirect");
                           ViewHelper::redirect(BASE_URL."newClient?error=2");
                       }else{
                            ViewHelper::redirect(BASE_URL."newClient?error=7");
                       }
                       
                         //registracija uspesna...prijavite se...
                   }else{
                       ViewHelper::redirect(BASE_URL."newClient?error=3");
                   }   
               }
               else{
                   ViewHelper::redirect(BASE_URL."newClient?error=1"); //gesli se ne ujemata
               }
                var_dump($data);
            }else{
                ViewHelper::redirect(BASE_URL."newClient?error=3");
                //throw  new InvalidArgumentException;
            }
              
          } else {
             ViewHelper::redirect(BASE_URL."newClient?error=6");
          }
        
        
        
    }
    
    public static function editOrderShow(){
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        if(self::checkValues($data)){
            //var_dump($data);
            //var_dump();
            echo ViewHelper::render("../../view/seller/editOrder.php", [
                 "all" => DB::getOneInvoice($data['id']),
                 "invoice" => DB::getInvoice($data['id']),
                 "client" => DB::getSelf(DB::getInvoice($data['id'])['id_oseba'])[0]
                 
             ]);
        }else{
            throw  new InvalidArgumentException;
        }
    }
    
    public static function editInvoice(){
         $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        if(self::checkValues($data)){
            //var_dump($data);
            //var_dump();
            echo ViewHelper::render("../../view/seller/EditInvoice.php", [
                 "all" => DB::getOneInvoice($data['id']),
                 "invoice" => DB::getInvoice($data['id']),
                 "client" => DB::getSelf(DB::getInvoice($data['id'])['id_oseba'])[0]
             ]);
        }else{
            throw  new InvalidArgumentException;
        }
    }
    
    public static function showCanceledInvoice(){
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        if(self::checkValues($data)){
            //var_dump($data);
            //var_dump();
            
            echo ViewHelper::render("../../view/seller/CanceledInvoice.php", [
                 "all" => DB::getOneInvoice($data['id']),
                 "invoice" => DB::getInvoice($data['id']),
                 "client" => DB::getSelf(DB::getInvoice($data['id'])['id_oseba'])[0]
                 
             ]);
        }else{
            throw  new InvalidArgumentException;
        }
    }
    
    public static function confirmOrder(){
         $rules = [
            "invoiceId" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        if(self::checkValues($data)){
            var_dump($data);
            try {
                DB::changeInvoiceStatus($data['invoiceId'], 2);
                ViewHelper::redirect(BASE_URL . "sellerPage?error=0");
            } catch (Exception $ex) {
                var_dump($ex->getMessage());
            }
        }else{
            throw  new InvalidArgumentException;
        }
    }
    
    public static function cancelOrder(){
         $rules = [
            "invoiceId" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        if(self::checkValues($data)){
            var_dump($data);
            try {
                DB::changeInvoiceStatus($data['invoiceId'], 3);
                ViewHelper::redirect(BASE_URL . "sellerPage?error=0");
            } catch (Exception $ex) {
                var_dump($ex->getMessage());
            }
        }else{
            throw  new InvalidArgumentException;
        }
    }
    
    public static function activateUser(){
         $rules = [
               "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1] //manj kot 0 ne sme bit id se mi zdi
            ],
        ];
        
        $data = filter_input_array(INPUT_GET, $rules);
        if(self::checkValues($data)){
            $user = DB::getSelf($data['id']);
            if(empty($user)){
                echo "no such user";
            }
            else{
                DB::changeOsebaStatus($data['id'], 4);
                echo "User activated Now you can login..";
                ?><a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "seller.php"). "../../mainPages/client/client.php/login"."?error=0") ?>">LOGIN</a> <?php
            }
        }else{
            throw new InvalidArgumentException;
        }
    }

    //helper function.....
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }
        
        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
}