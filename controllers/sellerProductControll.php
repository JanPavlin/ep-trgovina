<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class ProductControll{
     public static function editProduct(){
        //var_dump("se pride");
        $rules = [
            "productID" =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ],
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options'=>['min_range' => 0, 'max_range' =>12]
            ]
            
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        if(isset($data)){
             echo ViewHelper::render("../../view/seller/editProduct.php", [ 
                 "product" => DB::getOneProduct($data["productID"]), 
                 "productPicures" => DB::getSlika($data['productID']),
                 "error" => $data['error']
                    ]);
        }else{
            throw new InvalidArgumentException();
        }
    }
    
    public static function deleteProduct(){
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ],
            "deleteConfirm" => FILTER_REQUIRE_SCALAR
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        var_dump($data);
        if(self::checkValues($data)){
            DB::deleteIzdelek($data["id"]);
            ViewHelper::redirect(BASE_URL . "sellerPage?error=0");
        }else{
            ViewHelper::redirect(BASE_URL . "eidtProduct?productID=".$data['id']."&error=8");
        }
    }
    public static function changeProductStatus(){
          $rules = [
             "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "status" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1, 'max_range'=>2] //manj kot 0 ne sme bit id se mi zdi
            ],
        ];
        //var_dump($_POST['status']);
        $data = filter_input_array(INPUT_POST, $rules);
        var_dump($data);
        if(self::checkValues($data)){
            if($data['status'] == 1){
               DB::chnageStatusIzdelka($data['id'], 2);
            }else{
               DB::chnageStatusIzdelka($data['id'], 1);  
            }
            ViewHelper::redirect(BASE_URL . "eidtProduct?productID=".$data['id']."&error=6");
            
        }else{
              throw new InvalidArgumentException();
        }
    }
    public static function changeProductData(){
        $rules = [
            "id" => [
                 'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ],
            "product" => FILTER_SANITIZE_SPECIAL_CHARS,
            "description" => FILTER_SANITIZE_SPECIAL_CHARS,
            "price" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, "max_range" => 100]
            ]
        ]; 
        
         $data = filter_input_array(INPUT_POST, $rules);
         if(self::checkValues($data)){
             if($data['product'] == ""){
                 ViewHelper::redirect(BASE_URL . "eidtProduct?productID=".$data['id']."&error=1");
             }else{
                 var_dump($data);
                DB::changeDataIzdelek($data['id'], $data["product"], $data['description'], $data["price"]);
                ViewHelper::redirect(BASE_URL . "eidtProduct?productID=".$data['id']."&error=2");
                //var_dump($data);
             }
                          
         }else{
             ViewHelper::redirect(BASE_URL . "eidtProduct?productID=".$data['id']."&error=1"); //neuspesna sprememba
         }
    }
    
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }
        
        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
    public static function addPics(){
        $rules = [
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range'=>1]
            ]
        ];
        
        
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            if(isset($_FILES['UploadImage']['name']) && !empty($_FILES['UploadImage']['name'])){
                
                
                $uploaddir = '/home/ep/NetBeansProjects/ep-trgovina/static/img/';
                $uploadfile = $uploaddir . basename($_FILES['UploadImage']['name']);
                if (copy($_FILES['UploadImage']['tmp_name'], $uploadfile)) {
                    DB::insertSlika($data['id'], $_FILES['UploadImage']['name']);
                    //echo "File is valid, and was successfully uploaded.\n";
                    //var_dump(DB::getSlika($data['id']));
                    ViewHelper::redirect(BASE_URL . "eidtProduct?productID=".$data['id']."&error=12"); //success
                  } else {
                     ViewHelper::redirect(BASE_URL . "eidtProduct?productID=".$data['id']."&error=11"); //upload failed
                }
                
                
            }else{
                 ViewHelper::redirect(BASE_URL . "eidtProduct?productID=".$data['id']."&error=10"); //empty file
            }
              
        }else{
            throw new InvalidArgumentException;
        }     
    }
    public static function deletePicture(){
           $rules = [
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range'=>1]
            ],
             'productId'=> [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range'=>1]
            ],
            'picturePath'=> FILTER_SANITIZE_SPECIAL_CHARS
        ];
        
        
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            var_dump($data);
            try {
                DB::deleteProductSlika($data['productId'], $data['id']);
                ViewHelper::redirect(BASE_URL . "eidtProduct?productID=".$data['productId']."&error=0");
            } catch (Exception $ex) {
                var_dump($ex);
            }
        }else{
            throw new InvalidArgumentException;
        }
        
    }
}
