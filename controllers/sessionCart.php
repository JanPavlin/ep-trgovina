<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Cart{
    public static function getProductsInCart(){
         if (!isset($_SESSION["cart"]) || empty($_SESSION["cart"])) {
            return [];
        }
        
        $idProducts = array_keys($_SESSION['cart']); //pridobi vse kljuce izdelkov....
       
        $productsInBasket = DB::getAllProductInBasket($idProducts);
        
        $nov = [];
        foreach($productsInBasket as $key=> $product){
            //echo $_SESSION['cart'][$product['id_izdelka']];
            $product['quantity'] = $_SESSION['cart'][$product['id_izdelka']];
            ///dodaj notri....
            //var_dump($product);
            //var_dump($key);
            $nov[$key] = $product;
        }
       // var_dump($nov);
        
        return $nov;
        
    }
    
    public static function addProductToCart($productId){
        //TODO, PREVERI, CE PRODUKT SPLOH OBSTAJA V BAZI...
        $product = DB::getOneProduct($productId);
        if($product != null){
            if(isset($_SESSION["cart"][$productId])){
                $_SESSION["cart"][$productId] += 1;
            }else{
                $_SESSION["cart"][$productId] = 1;
            }
        }else{
            //Ce se to zgodi en neki dela po svoje, definitivno....
            throw new InvalidArgumentException;
        }
    }
    
    public static function updateProductInCart($productId, $quantity){
        $product = DB::getOneProduct($productId);
        $qant = intval($quantity);
        if($product != null){
            if ($qant <= 0) {
                var_dump("pa kaj je s tabo....");
                throw new InvalidArgumentException;
            } else {
                $_SESSION["cart"][$productId] = $qant;
            }
        }else{
            //delas neumnosti, nekje v apliaciji....
            throw new InvalidArgumentException;
        }
    }
    
    public static function deleteOneProductInCart($productId){
        $product = DB::getOneProduct($productId);
        if($product != null){
            if(isset($_SESSION['cart'][$productId])){
                unset($_SESSION['cart'][$productId]);
            }else{
                var_dump("zakaj odstranjujes nekaj cesar ni OMG");
                throw new InvalidArgumentException;
            }
        }else{
            //pa ti si res model...
            throw new InvalidArgumentException;
        }
    }
    
    public static function purgeCart(){
         unset($_SESSION["cart"]);
    }
    
    public static function calculateTotal(){
        $total = 0.0;
        //TODO PRIDBI VSE AKTIVNE IZDELKE IN IZ NJIH ZRACUNAJ GLEDE NA KOLICINO IN CENO SKUPEN ZNESEK IN GA VRNI...
            if(!isset($_SESSION["cart"]) || empty($_SESSION["cart"])){
                return 0.0;
            }else{
                $idProducts = array_keys($_SESSION['cart']); //pridobi vse kljuce izdelkov....
                //var_dump($idProducts);
                $productsInBasket = DB::getAllProductInBasket($idProducts); //se sprehod cez vse izdelke in sestevek v total.
                foreach($productsInBasket as $key=>$product){
                    
                    $total += ($product['postavka'] * $_SESSION['cart'][$product['id_izdelka']]);
                }
            }
            
        return $total;
    }
    
}

