<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class sellerController{
    public static function getOneSeller(){
        $rulesGet = [
             "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ],
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range'=>6]
            ]
        ];
        //var_dump($_GET['id']);
        $data = filter_input_array(INPUT_GET, $rulesGet);
        //var_dump($data)
        if(self::checkValues($data)){
           // var_dump($data);
            echo ViewHelper::render("../../view/admin/changeSeller.php", [
            "sellerInfo" => DB::getSelf($data['id'])[0],
            "error" => $data["error"]
            ]);
        }else{
             throw new InvalidArgumentException();
        }
       
    }
    
    public static function changeSellerData(){
          $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "name" => FILTER_SANITIZE_SPECIAL_CHARS,
            "surname" => FILTER_SANITIZE_SPECIAL_CHARS,
            "username" => FILTER_VALIDATE_EMAIL //filteriranje emaila.
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            if($data['name']=='' || $data['surname']=='' || $data["username"] == ''){
             ViewHelper::redirect(BASE_URL . "editSeller?id=".$data['id']."&error=0");   
            }else{
                $dataOseba = DB::getSelf($data['id'])[0];
                var_dump($data, "klicar se mora funkcija za spremembo...");
                DB::ChangeDataOseba($data['id'], $data['name'], $data['surname'], $dataOseba['id_statusosebe'], $data['username'], $dataOseba['geslo']);
                ViewHelper::redirect(BASE_URL . "editSeller?id=".$data['id']."&error=2");
            }
            
        }else{
             ViewHelper::redirect(BASE_URL . "editSeller?id=".$data['id']."&error=1");  //spremba nemogoca, ker ni sel cez filter
        }
    }
    public static function changeSellerStatus(){
        $rules = [
             "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "status" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 2, 'max_range'=>3] //manj kot 0 ne sme bit id se mi zdi
            ],
        ];
        //var_dump($_POST['status']);
        $data = filter_input_array(INPUT_POST, $rules);
        var_dump($data);
        if(self::checkValues($data)){
            $dataOseba = DB::getSelf($data['id'])[0];
            var_dump($dataOseba);
            if($data['status'] == 3){
                
                var_dump("Nov status 2 -->potreben klic v bazo.");
                var_dump ($data['id'], $dataOseba['ime'], $dataOseba['priimek'], '2', $dataOseba['username'], $dataOseba['geslo']);
                DB::ChangeDataOseba($data['id'], $dataOseba['ime'], $dataOseba['priimek'], 2, $dataOseba['username'], $dataOseba['geslo']);
            }else{
                DB::ChangeDataOseba($data['id'], $dataOseba['ime'], $dataOseba['priimek'], 3, $dataOseba['username'], $dataOseba['geslo']);
                var_dump("Nov status 3 --> potreben klic v bazo.");
            }
            ViewHelper::redirect(BASE_URL . "editSeller?id=".$data['id']."&error=6");
            
        }else{
              throw new InvalidArgumentException();
        }
    }
    
    public static function changePasswordSeller(){
           $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "passwordNew" => FILTER_SANITIZE_SPECIAL_CHARS,
            "passwordConfirm" => FILTER_SANITIZE_SPECIAL_CHARS
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        var_dump($data);
        if(isset($data)){
            //var_dump($user);
            if($data['passwordNew'] == $data['passwordConfirm']){
                //var_dump("vse vredu, potreben je klic da se geslo spremeni");
                DB::changePassword($data['id'], $data['passwordNew']);
                ViewHelper::redirect(BASE_URL . "editSeller?id=".$data['id']."&error=5");
            }
            else{
                ViewHelper::redirect(BASE_URL . "editSeller?id=".$data['id']."&error=4");
            }
            
        }else{
            //var_dump("err");
            //ViewHelper::redirect(BASE_URL . "adminPage");
            throw new InvalidArgumentException();
        }
    }
    public static function createSellerForm (){
        $rules = [
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range'=>5]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        //var_dump($data);
        if(isset($data)){
            echo ViewHelper::render("../../view/admin/createSeller.php", [
                "error" => $data['error']
            ]);
        }else{
            throw new InvalidArgumentException();
        }
        
    }
    
    public static function createSeller(){
        $rules = [
            "status" =>  [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 2, 'max_range' => 3] //manj kot 0 ne sme bit id se mi zdi
            ],
            "name" => FILTER_SANITIZE_SPECIAL_CHARS,
            "surname" => FILTER_SANITIZE_SPECIAL_CHARS,
            "username" => FILTER_VALIDATE_EMAIL, //filteriranje emaila.
            "password1" => FILTER_SANITIZE_SPECIAL_CHARS,
            "password2" =>FILTER_SANITIZE_SPECIAL_CHARS
        ];
        //var_dump($_POST['status']);
        $data = filter_input_array(INPUT_POST, $rules);
        //var_dump($data);
        if(self::checkValues($data)){
            if($data['name'] == "" ||$data['surname'] == "" || $data['username'] == "" || $data['password1'] == "" || $data['password2'] == ""){
                 ViewHelper::redirect(BASE_URL ."newSeller?error=1");   
            }else if($data['password1'] != $data['password2'] ){
                ViewHelper::redirect(BASE_URL ."newSeller?error=2");
            }else{
                //uspesno preverjanje, potreben je klic funkcije....
                DB::registerProdajalec($data['name'], $data['surname'], $data['username'], $data['password1'], $data['status'] ,$_SESSION['admin']);
                ViewHelper::redirect(BASE_URL ."newSeller?error=3");
            }
            var_dump($data);  
        }else{
            throw new InvalidArgumentException();
        }
    }

    //helper function.....
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }
        
        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
}
