<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class anonymousUser{
    public static function indexPageUser(){   
        $allProducts = DB::getIzdelekActive();
        $allNew = [];
        foreach($allProducts as $key=> $product){
            //var_dump($product);
            $productScoreAll = DB::allOcenaZaProduct($product['id_izdelka']);
            $score = 0.0;
            foreach($productScoreAll as $key=>$one){
                $score += $one['vrednost'];
            }
            if(!empty($productScoreAll)){
                $score /= count($productScoreAll);
            }
            $product['score'] = $score;
             $pictures = DB::getSlika($product['id_izdelka']);
            if(!empty($pictures)){
                $product['slika'] = $pictures[0]['potSlike'];
            }else{
                $product['slika'] = null;
            }
            $allNew[$key] = $product;
            //var_dump($product);
        }
        echo ViewHelper::render("../../view/products.php", 
                [ "allProducts" => $allNew 
                
                ]);
    }
    
    
    
    public static function getOneProduct(){
      $rules = [
            "productID" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        
        
        if(self::checkValues($data)){
            $productScoreAll = DB::allOcenaZaProduct($data['productID']);
            $score = 0.0;
            foreach($productScoreAll as $key=>$one){
                $score += $one['vrednost'];
            }
            if(!empty($productScoreAll)){
                $score /= count($productScoreAll);
            }
            //var_dump($productScoreAll);
            //echo $score;
            //var_dump($canScore);
            echo ViewHelper::render("../../view/productDetail.php", 
                    [ "product" => DB::getOneProduct($data["productID"]),
                       "productPicures" => DB::getSlika($data['productID']),
                      "score" => $score,
                    
                     ]);
        }
    }
    
    
    
     ///Helper funkcije....
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }
        
        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
    
    
    private static function getRulesLogin() {
        return [
           //
        ];
    }
    
}