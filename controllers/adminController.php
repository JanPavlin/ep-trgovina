<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AdminController{
    public static function getAllAdmin(){
        $rules = [
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range'=>5]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        //var_dump($data);
        if(isset($data)){
            echo ViewHelper::render("../../view/admin/adminPage.php", [
            "userInfo" => DB::getSelf($_SESSION["admin"])[0],
            "sellerData" => DB::getAllProdajalec(),
            "error" => $data['error']
            ]);
        }
        else{
            var_dump("errr");
        }
       
    }
    
    public static function logOut(){
         $rules = [
            'logOut_confirm' => FILTER_REQUIRE_SCALAR,
          
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            //unici sejo in preusmeri nazaj..
            adminSession::destroySession();
            ViewHelper::redirect(BASE_URL);
        }else{
             ViewHelper::redirect(BASE_URL . "adminPage?error=3");
        }
    }
    public static function changeData(){
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "name" => FILTER_SANITIZE_SPECIAL_CHARS,
            "surname" => FILTER_SANITIZE_SPECIAL_CHARS,
            "username" => FILTER_VALIDATE_EMAIL //filteriranje emaila.
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            if($data['name']=='' || $data['surname']=='' || $data["username"] == ''){
             ViewHelper::redirect(BASE_URL . "adminPage?error=0");   
            }else{
                var_dump($data, "klicar se mora funkcija za spremembo...");
                $dataOseba = DB::getSelf($data['id'])[0];
                DB::ChangeDataOseba($data['id'], $data['name'], $data['surname'], $dataOseba['id_statusosebe'], $data['username'], $dataOseba['geslo']);
                ViewHelper::redirect(BASE_URL . "adminPage?error=2");
            }
            
        }else{
             ViewHelper::redirect(BASE_URL . "adminPage?error=1");  //spremba nemogoca, ker ni sel cez filter
        }
    }
    
    public static function changePassword(){
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "passwordOld" => FILTER_SANITIZE_SPECIAL_CHARS,
            "passwordNew" => FILTER_SANITIZE_SPECIAL_CHARS,
            "passwordConfirm" => FILTER_SANITIZE_SPECIAL_CHARS
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        var_dump($data);
        if(isset($data)){
            $user = DB::getSelf($_SESSION["admin"])[0];
            var_dump($user);
            if($user['geslo'] == sha1($data['passwordOld'])){
                if($data['passwordNew'] == $data['passwordConfirm']){
                    //var_dump("vse vredu, potreben je klic da se geslo spremeni");
                    DB::changePassword($_SESSION['admin'], $data['passwordNew']);
                    ViewHelper::redirect(BASE_URL . "adminPage?error=5");
                }
                else{
                    ViewHelper::redirect(BASE_URL . "adminPage?error=4");
                }
            }else{
                ViewHelper::redirect(BASE_URL . "adminPage?error=4");
            }
        }else{
            var_dump("err");
            //ViewHelper::redirect(BASE_URL . "adminPage");
        }
    }
    
    public static function showLogs(){
        
        echo ViewHelper::render("../../view/admin/adminLogs.php", [
            "logs" => DB::getLog()
            ]);
    }
    
    //helper function.....
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }
        
        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
}