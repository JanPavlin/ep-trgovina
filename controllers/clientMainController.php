<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ClientMain{
    public static function clienMainPage(){
        //var_dump("client main");
        
        $allProducts = DB::getIzdelekActive();
        $allNew = [];
        foreach($allProducts as $key=> $product){
            //var_dump($product);
            $productScoreAll = DB::allOcenaZaProduct($product['id_izdelka']);
            $score = 0.0;
            foreach($productScoreAll as $key=>$one){
                $score += $one['vrednost'];
            }
            if(!empty($productScoreAll)){
                $score /= count($productScoreAll);
            }
            $product['score'] = $score;
             $pictures = DB::getSlika($product['id_izdelka']);
            if(!empty($pictures)){
                $product['slika'] = $pictures[0]['potSlike'];
            }else{
                $product['slika'] = null;
            }
            $allNew[$key] = $product;
            //var_dump($product);
        }
        //var_dump($allNew);
         echo ViewHelper::render("../../view/client/clientMainPage.php", [
             "allProducts" => $allNew,
             "client" => DB::getSelf($_SESSION['client'])[0],
             "cart" => Cart::getProductsInCart(),
             "total" => Cart::calculateTotal(),
             "invoices" => DB::getOsebaNarocila($_SESSION['client'])
         ]);
    }
    
    public static function showInvoice(){
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        if(self::checkValues($data)){
            //var_dump($data);
            //var_dump();
            echo ViewHelper::render("../../view/client/showInvoice.php", [
                 "all" => DB::getOneInvoice($data['id']),
                 "invoice" => DB::getInvoice($data['id']),
                  "client" => DB::getSelf(DB::getInvoice($data['id'])['id_oseba'])[0]
                 
             ]);
        }else{
            throw  new InvalidArgumentException;
        }
    }
    
    public static function ProductDetail(){
         $rules = [
            "productID" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        
        
        if(self::checkValues($data)){
            $productScoreAll = DB::allOcenaZaProduct($data['productID']);
            $score = 0.0;
            $canScore = true;
            foreach($productScoreAll as $key=>$one){
                if($one['id_osebe'] == $_SESSION['client']){
                    $canScore = false;
                    
                }
                $score += $one['vrednost'];
            }
            if(!empty($productScoreAll)){
                $score /= count($productScoreAll);
            }
            //var_dump($productScoreAll);
            //echo $score;
            //var_dump($canScore);
            echo ViewHelper::render("../../view/client/productDetailAndScore.php", 
                    [ "product" => DB::getOneProduct($data["productID"]),
                       "productPicures" => DB::getSlika($data['productID']),
                      "score" => $score,
                       "canScore" => $canScore
                     ]);
        }
    }
    
    public static function  addToCart(){
        $rules = [
            "id" =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ]
        ];
        
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            Cart::addProductToCart($data['id']);
            
            ViewHelper::redirect(BASE_URL . "clientMain"); //ko je to uspesno naredi preusmeritev nazaj na original stran...
        }else{
            throw  new InvalidArgumentException;
        }
        
    }
    
    public static function updateProductInCart(){
        $rules = [
            "id" =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ],
            "amount" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1] //kolicina manjsa od 0 je nekaj hudo narobe...
            ]
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        //var_dump($data);
         if(self::checkValues($data)){
             Cart::updateProductInCart($data['id'], $data['amount']);
             ViewHelper::redirect(BASE_URL . "clientMain"); //ko je to usesno naredi novo stanje...
        }else{
             throw  new InvalidArgumentException; //nekaj hudo narobe
        }
    }
    
    public static function deleteProductFromCart(){
         $rules = [
            "id" =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ]
        ];
        
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            Cart::deleteOneProductInCart($data['id']);
            ViewHelper::redirect(BASE_URL . "clientMain"); //ko je to uspesno naredi preusmeritev nazaj na original stran...
        }else{
            throw  new InvalidArgumentException;
        }
    }
    public static function deleteWholeCart(){
        Cart::purgeCart(); //zbrisi in to je to...preusmeri....
        ViewHelper::redirect(BASE_URL . "clientMain");
    }

    public static function editCustomer(){
        //var_dump("PRIDE ZA SPREMINJAT STRANKO!!!");
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "error" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range' =>8] //manj kot 0 ne sme bit id se mi zdi
            ]
        ];  
        
        $data = filter_input_array(INPUT_GET, $rules);
        //var_dump($data);
        if(isset($data)){
             
            echo ViewHelper::render("../../view/seller/customerEdit.php", [
                "customerMore" =>DB::getSelfStranka($data['id']),
                "customerBasic" => DB::getSelf($data['id'])[0],
                "error" => $data['error']
            ]);
        }else{
            throw new InvalidArgumentException;
        }
        
    }
    /*
     * 
     *  <p><label>Postal code: <input type="text" name="" value="<?= $customerMore["posta"] ?>" required/></label></p>
        <p><label>City: <input type="text" name="City" value="<?= $customerMore["kraj"] ?>" required/></label></p>
        <p><label>Street: <input type="text" name="Street" value="<?= $customerMore["ulica"] ?>" required/></label></p>
        <p><label>Street number: <input type="text" name="Streetnumber" value="<?= $customerMore["stevilka"] ?>" required/></label></p>
        <p><label>TelNumber: <input type="tel" name="telephoneNumber" value="<?= $customerMore["telefonska"] ?>" required/></label></p>
        <p><button>Change Customer Data</button></p>
     * 
     * 
     *Post  City  Street  Streetnumber
     */
    public static function changeClientData(){
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "name" => FILTER_SANITIZE_SPECIAL_CHARS,
            "surname" => FILTER_SANITIZE_SPECIAL_CHARS,
            "username" => FILTER_VALIDATE_EMAIL, //filteriranje emaila.
            "Post" => FILTER_VALIDATE_INT,
            "City" => FILTER_SANITIZE_SPECIAL_CHARS,
            "Street" => FILTER_SANITIZE_SPECIAL_CHARS,
            "Streetnumber" => FILTER_SANITIZE_SPECIAL_CHARS,
            "telephoneNumber" => FILTER_SANITIZE_SPECIAL_CHARS
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        var_dump($data);
        var_dump(strlen($data['Street']) <= 49);
        if(self::checkValues($data)){
            if($data['name']=='' || $data['surname']=='' || $data["username"] == '' || $data['Post'] == '' || $data['Street'] == ''
                    || $data['Streetnumber'] == '' || $data['City'] == '' ){
             ViewHelper::redirect(BASE_URL . "editCustomer?id=".$data['id']."&error=0");   
            }else{
                if(strlen($data['telephoneNumber']) == 9){
                    if(strlen($data['Street']) <= 49 && strlen($data['name']) <= 49 && strlen($data['surname']) <= 49 && strlen($data['City']) <= 49){
                         $dataOseba = DB::getSelf($data['id'])[0];
                        //var_dump($data, "klicar se mora funkcija za spremembo...");
                        DB::ChangeDataOseba($data['id'], $data['name'], $data['surname'], $dataOseba['id_statusosebe'], $data['username'], $dataOseba['geslo']);
                        DB::ChangeDataStranka($data['id'], $data['telephoneNumber'], $data['Post'], $data['City'], $data['Street'], $data['Streetnumber'] );

                        ViewHelper::redirect(BASE_URL . "editCustomer?id=".$data['id']."&error=2");
                    }
                    else{
                        ViewHelper::redirect(BASE_URL . "editCustomer?id=".$data['id']."&error=8");   
                    }
                    
                }else{
                    ViewHelper::redirect(BASE_URL . "editCustomer?id=".$data['id']."&error=7");   
                }
                
            }
            
        }else{
             ViewHelper::redirect(BASE_URL . "editCustomer?id=".$data['id']."&error=1");  //spremba nemogoca, ker ni sel cez filter
        }
    }
    
    public static function changeClientStatus(){
        $rules = [
             "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "status" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 4, 'max_range'=>5] //manj kot 0 ne sme bit id se mi zdi
            ],
        ];
        //var_dump($_POST['status']);
        $data = filter_input_array(INPUT_POST, $rules);
        var_dump($data);
        if(self::checkValues($data)){
            $dataOseba = DB::getSelf($data['id'])[0];
            var_dump($dataOseba);
            if($data['status'] == 5){
                
                var_dump("Nov status 4 -->potreben klic v bazo.");
                var_dump ($data['id'], $dataOseba['ime'], $dataOseba['priimek'], '4', $dataOseba['username'], $dataOseba['geslo']);
                //DB::ChangeDataOseba($data['id'], $dataOseba['ime'], $dataOseba['priimek'], 4, $dataOseba['username'], $dataOseba['geslo']);
                DB::changeOsebaStatus($data['id'], 4);
            }else{
                //DB::ChangeDataOseba($data['id'], $dataOseba['ime'], $dataOseba['priimek'], 5, $dataOseba['username'], $dataOseba['geslo']);
                DB::changeOsebaStatus($data['id'], 5);
                var_dump("Nov status 5 --> potreben klic v bazo.");
            }
            ViewHelper::redirect(BASE_URL . "editCustomer?id=".$data['id']."&error=6");
            
        }else{
              throw new InvalidArgumentException();
        }
    }
    
    public static function changePasswordClient(){
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0] //manj kot 0 ne sme bit id se mi zdi
            ],
            "passwordOld" => FILTER_SANITIZE_SPECIAL_CHARS,
            "passwordNew" => FILTER_SANITIZE_SPECIAL_CHARS,
            "passwordConfirm" => FILTER_SANITIZE_SPECIAL_CHARS
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        var_dump($data);
        if(isset($data)){
            //var_dump($user);
            if($data['passwordNew'] == $data['passwordConfirm']){
                //var_dump("vse vredu, potreben je klic da se geslo spremeni");
                DB::changePassword($data['id'], $data['passwordNew']);
                ViewHelper::redirect(BASE_URL . "editCustomer?id=".$data['id']."&error=5");
            }
            else{
                ViewHelper::redirect(BASE_URL . "editCustomer?id=".$data['id']."&error=4");
            }
            
        }else{
            //var_dump("err");
            //ViewHelper::redirect(BASE_URL . "adminPage");
            throw new InvalidArgumentException();
        }
    }

    public static function scoreProduct(){
        $rules = [
            "score" =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range' => 10]
            ],
             "id" =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        
        $data = filter_input_array(INPUT_POST, $rules);
        if(self::checkValues($data)){
            //var_dump($data);
            DB::addOcena($data['id'], $data['score'], $_SESSION['client']);
            ViewHelper::redirect(BASE_URL . "detailsAndScore?productID=".$data['id']);
        }else{
            throw new InvalidArgumentException;
        }
    }
    
    public static function activateUser(){
        $rules = [
               "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1] //manj kot 0 ne sme bit id se mi zdi
            ],
        ];
        
        $data = filter_input_array(INPUT_GET, $rules);
        if(self::checkValues($data)){
            $user = DB::getSelf($data['id']);
            if(empty($user)){
                echo "no such user";
            }
            else{
                DB::changeOsebaStatus($data['id'], 4);
                echo "User activated Now you can login..";
                ?><a href="<?= htmlspecialchars(BASE_URL.'login?error=0')  ?>">LOGIN</a> <?php
            }
        }else{
            throw new InvalidArgumentException;
        }
    }
    
      //helper function.....
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }
        
        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
}
