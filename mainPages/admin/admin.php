<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ 
                header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
require_once ("../../certs/verifyClientCert.php");
//var_dump(VerifyCert::verify("Admin"));
if(VerifyCert::verify("Admin") == false){
    echo "Incorrect certificate for admin"
    ?><a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "admin.php"). "../../index.php")  ?>">Back to main</a> <?php
    die();
}

session_start();
 
 
    require_once("../ViewHelper.php");
    require_once("../../controllers/authentication-controller.php");
    require_once ("../../resources/database/database.php");
    require_once("./adminSession.php");
    require_once("../../controllers/adminController.php");
    require_once("../../controllers/sellerController.php");
     
    define("BASE_URL", htmlspecialchars($_SERVER["SCRIPT_NAME"] . "/"));
    define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "admin.php") . "../../static/img/");
    define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "admin.php") . "../../static/css/");
    define("JS_URL", rtrim($_SERVER["SCRIPT_NAME"], "admin.php") . "../../static/javascript/");

    $path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";
    
//indeksu samo povemo kam gremo ko pridemo na zacetno stran, glede na parametre....
$urls = [
    "" => function () {
        if(isset($_SESSION["admin"])){
            //echo "notr";
            ViewHelper::redirect(BASE_URL . "adminPage?error=3");
        }else{
            AuthController::indexPageAdmin();
        }
    },
    "adminLogin" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            AuthController::AdminLogin();
        }
    },
    "adminPage" => function (){
        if(isset($_SESSION["admin"])){
            AdminController::getAllAdmin();
        }else{
            ViewHelper::redirect(BASE_URL);
        }   
    },
    "logOut" => function(){
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            AdminController::logOut();
        }
        
    },
    "updateAdminData" => function(){
         if($_SERVER["REQUEST_METHOD"] == "POST"){
            AdminController::changeData();
        }
    },
    "changePassword" => function(){
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            AdminController::changePassword();
        } 
    },
    "editSeller" =>function(){
        if($_SERVER["REQUEST_METHOD"] == "GET"){
            sellerController::getOneSeller();
        }
    },
    "updateSellerData" => function(){
       if($_SERVER["REQUEST_METHOD"] == "POST"){
           sellerController::changeSellerData();
        }  
    },
    "changeStatusSeller" => function(){
       if($_SERVER["REQUEST_METHOD"] == "POST"){
           sellerController::changeSellerStatus();
        } 
    },
    "changePasswordSeller" => function(){
        sellerController::changePasswordSeller();
    },
    "newSeller" => function(){
        
        if($_SERVER["REQUEST_METHOD"] == "GET"){
           sellerController::createSellerForm();
        } else if($_SERVER["REQUEST_METHOD"] == "POST"){
            sellerController::createSeller();
        }
        
    },
    "getLog" => function(){
        if($_SERVER["REQUEST_METHOD"] == "GET"){
            AdminController::showLogs();
        }
         
    }
   
  
];
    
try {
    if (isset($urls[$path])) {
        $urls[$path]();
    } else {
                        header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        echo "No controller for '$path'";
    }
} catch (InvalidArgumentException $e) {
    ViewHelper::error404();
} catch (Exception $e) {
                    header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
    echo "An error occurred: <pre>$e</pre>";
} 

//echo "123";

