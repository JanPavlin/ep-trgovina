<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
                header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
 
require_once ("../../resources/database/database.php");
require_once ("../../certs/verifyClientCert.php");

//var_dump(VerifyCert::verify("Admin"));
if(VerifyCert::verify("Seller") == false){
    echo "can't authorize seller with this certificate..."
    ?><a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "seller.php"). "../../index.php")  ?>">Back to main</a> <?php
    die();
}

session_start();

    require_once("../ViewHelper.php");
    require_once("../../controllers/authentication-controller.php");
    require_once("./sellerSession.php");
    require_once("../../controllers/sellerMain.php");
    require_once("../../controllers/sellerProductControll.php");
    require_once "../../recaptchalib.php";
    require_once "../../PHPMailer/PHPMailerAutoload.php";
    define("BASE_URL", htmlspecialchars($_SERVER["SCRIPT_NAME"] . "/"));
    define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "seller.php") . "../../static/img/");
    define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "seller.php") . "../../static/css/");
    define("JS_URL", rtrim($_SERVER["SCRIPT_NAME"], "seller.php") . "../../static/javascript/");
    require_once("../../controllers/clientMainController.php");

    $path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";
    
//indeksu samo povemo kam gremo ko pridemo na zacetno stran, glede na parametre....
$urls = [
    "" => function () {
        if(isset($_SESSION["seller"])){
            ViewHelper::redirect(BASE_URL."sellerPage?error=0");
        }else{
            AuthController::indexPageSeller();
        }
        
    },
    "login" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            AuthController::sellerLogin();
        }
    },
    "logOut" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            SellerMain::logOut();
        }
    },
    "sellerPage" => function(){
        if(isset($_SESSION['seller'])){
            //var_dump("welcome seller");
            SellerMain::mainSeller();
        }else{
             ViewHelper::redirect(BASE_URL);
        }
    },
    "newProduct" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            SellerMain::newProduct();
        }else if($_SERVER["REQUEST_METHOD"] == "GET"){
            SellerMain::newProductForm();
        }
    },
   "eidtProduct" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            ProductControll::editProduct();
        } 
   },
   "editCustomer"=> function(){
       if ($_SERVER["REQUEST_METHOD"] == "GET") {
            ClientMain::editCustomer();
        }
   },
   "updateCustomerData" => function(){
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
            ClientMain::changeClientData();
        }  
   },
   "changeStatusCustomer" => function(){
       if ($_SERVER["REQUEST_METHOD"] == "POST") {
            ClientMain::changeClientStatus();
        }  
   },
   "changePasswordCustomer" => function(){
     if ($_SERVER["REQUEST_METHOD"] == "POST") {
            ClientMain::changePasswordClient();
        }  
   }, 
    "changeSellerData" => function(){
       if ($_SERVER["REQUEST_METHOD"] == "POST") {
            SellerMain::chnageData();
        }
    },
    "changeSellerPassword" => function(){
       if ($_SERVER["REQUEST_METHOD"] == "POST") {
            SellerMain::changePasswordSellerMain();
        } 
    },
    "updateProduct" => function(){
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
            ProductControll::changeProductData();
        }  
    },
    "prductStatusChange" => function(){
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
            ProductControll::changeProductStatus();
        }  
    },
    "deleteProduct" => function(){
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
            ProductControll::deleteProduct();
        }  
    },
    "newClient" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            SellerMain::newClientRegister();
        } else{
            SellerMain::newClient();
        }
          
    },
    "editOrder" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            SellerMain::editOrderShow();
        }
        
    },
    "confirmOrder" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            SellerMain::confirmOrder();
        }
        
    },
    "cancelOrder" => function(){
         if ($_SERVER["REQUEST_METHOD"] == "GET") {
            SellerMain::cancelOrder();
        }
    },
    "editInvoice" => function(){
         if ($_SERVER["REQUEST_METHOD"] == "GET") {
            SellerMain::editInvoice();
        }
    },
    "showInvoice" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            SellerMain::showCanceledInvoice();
        }
    },
    "activate" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            SellerMain::activateUser();
        }
    },
    "addPicture" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            ProductControll::addPics();
        }
        
    },
    "deletePicture" =>function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            ProductControll::deletePicture();
        }
    }
   
  
];
    
try {
    if (isset($urls[$path])) {
        $urls[$path]();
    } else {
                        header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        echo "No controller for '$path'";
    }
} catch (InvalidArgumentException $e) {
    
    ViewHelper::error404();
} catch (Exception $e) {
                    header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
    echo "An error occurred: <pre>$e</pre>";
} 

//echo "123";

