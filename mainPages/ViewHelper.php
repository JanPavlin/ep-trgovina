<?php

class ViewHelper {
    
    //Uporaba ko kontroler hoce prikazat dolocen pogled s podanimi parametri.....
    public static function render($file, $variables = array()) {
        extract($variables); //senzam spremenljiv, ki jih dn potegne
                        header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        ob_start(); //outbut buffer start;
        include($file);
        return ob_get_clean(); //tukaj se vsi drugi ukazi izvršijo.
    }

    // Redirect na url
    public static function redirect($url) {
        header("Location: " . $url);
    }
    
    public static function renderJSON($data, $httpResponseCode = 200) {
        header('Content-type:application/json;charset=utf-8');
        http_response_code($httpResponseCode);
        //echo
        //return json_encode((object)$data);
        return json_encode($data);
    }

    // Errror 404 not found!!!
    public static function error404() {
                        header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        header('This is not the page you are looking for', true, 404);
        $html404 = sprintf("<!doctype html>\n" .
                "<title>Error 404: Page does not exist</title>\n" .
                "<h1>Error 404: Page does not exist</h1>\n" .
                "<p>The page <i>%s</i> does not exist.</p>", $_SERVER["REQUEST_URI"]);

        echo $html404;
    }
    
    public static function displayError($exception, $debug = false) {
        header('An error occurred.', true, 400);
                        header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        if ($debug) {
            $hmtl = sprintf("<!doctype html>\n" .
                    "<title>Error: An application error.</title>\n" .
                    "<h1>Error: An application error</h1>\n" .
                    "<p>The page <i>%s</i> returned an error:" .
                    "<blockquote><pre>%s</pre></blockquote></p>", $_SERVER["REQUEST_URI"], $exception);
        } else {
            $hmtl = sprintf("<!doctype html>\n" .
                    "<title>Error: An application error.</title>\n" .
                    "<h1>Error: An application error</h1>\n" .
                    "<p>The page <i>%s</i> returned an error.", $_SERVER["REQUEST_URI"]);
        }

        echo $hmtl;
    }

}