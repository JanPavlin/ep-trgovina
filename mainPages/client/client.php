<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ 


session_start();

        header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
    
    require_once("../ViewHelper.php");
    require_once("../../controllers/authentication-controller.php");
    require_once ("../../resources/database/database.php");
    require_once("../../controllers/clientMainController.php");
    require_once ("clientSession.php");
    require_once ("../../controllers/sessionCart.php");
    require_once ("../../controllers/invoceControll.php");
    require_once "../../recaptchalib.php";
    require_once "../../PHPMailer/PHPMailerAutoload.php";
    define("BASE_URL", htmlspecialchars($_SERVER["SCRIPT_NAME"] . "/"));
    define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "client.php") . "../../static/img/");
    define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "client.php") . "../../static/css/");
    define("JS_URL", rtrim($_SERVER["SCRIPT_NAME"], "client.php") . "../../static/javascript/");

    $path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";
    
    
//indeksu samo povemo kam gremo ko pridemo na zacetno stran, glede na parametre....
$urls = [
    "" => function () {
        ViewHelper::redirect(BASE_URL."login");
    },
    "login" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            AuthController::clientLogin();
            
        }else{
            if(isset($_SESSION['client'])){
                ViewHelper::redirect(BASE_URL . "clientMain");   
            }else{
                AuthController::indexPageClient();
            }
        }
    },
    "register" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //AuthController::sellerLogin();
            AuthController::clientRegister();
        }else{
            if(isset($_SESSION['client'])){
                ViewHelper::redirect(BASE_URL . "clientMain");   
            }else{
                AuthController::indexPageClientRegister();
            }
            
        }
        
    },
    "clientLogOut" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //AuthController::sellerLogin();
            AuthController::clientLogOut();
        }
    },
    "clientMain" => function(){
        if(isset($_SESSION['client'])){
            ClientMain::clienMainPage();
        }
        else{
            ViewHelper::redirect(BASE_URL . "login?"."error=0");   
        }
        
    },
     "basketAdd" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //AuthController::sellerLogin();
            ClientMain::addToCart();
        }
     },
    "updateCart" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //AuthController::sellerLogin();
            ClientMain::updateProductInCart();
        }  
    },
    "deleteFromBasket" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //AuthController::sellerLogin();
            ClientMain::deleteProductFromCart();
        } 
    },
    "purgeCart" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //AuthController::sellerLogin();
            ClientMain::deleteWholeCart();
        } 
    },
    "checkout" => function(){
        Invoice::showCheckOut();
    },
    "purchase" => function(){
        Invoice::purchase(); 
    },
    "detailsAndScore" => function(){
        ClientMain::ProductDetail();
    },
    "editCustomer" => function(){
        ClientMain::editCustomer();
    },
    "updateCustomerData" => function(){
         if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //AuthController::sellerLogin();
            ClientMain::changeClientData();
        }
    },
    "changePasswordCustomer" => function(){
         if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //AuthController::sellerLogin();
            ClientMain::changePasswordClient();
        }
    },
    "showOneInvoice" => function(){
        ClientMain::showInvoice();
    },
    "scoreProduct" => function(){
         if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //AuthController::sellerLogin();
            ClientMain::scoreProduct();
        }
    },
    "regisactivate" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            //AuthController::sellerLogin();
            ClientMain::activateUser();
        }
    },
    "deleteAcc" => function(){
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            //AuthController::sellerLogin();
            AuthController::deleteAcc();
        }
    }
  
];
    
try {
    if (isset($urls[$path])) {
        $urls[$path]();
    } else {
                        header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        echo "No controller for '$path'";
    }
} catch (InvalidArgumentException $e) {
    ViewHelper::error404();
} catch (Exception $e) {
                    header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
    echo "An error occurred: <pre>$e</pre>";
} 

//echo "123";

