<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'authorizedSellers.php';

class VerifyCert{
    public static function verify($param){
        
         $authorized_users;
         if($param == "Seller"){
             Sellers::activeSellers(DB::getActiveProdajalec());
             $authorized_users = Sellers::getSellers();
            // var_dump($authorized_users);
         }else if($param == "Admin"){
             $authorized_users = ["admin"];
         }else{
             die('Wrong params for authentication!!!');
         }
         
    $client_cert = filter_input(INPUT_SERVER, "SSL_CLIENT_CERT");

    if ($client_cert == null) {
        die('err:  SSL_CLIENT_CERT not set.');
    }

    $cert_data = openssl_x509_parse($client_cert);
    $commonname = (is_array($cert_data['subject']['CN']) ?
                    $cert_data['subject']['CN'][0] : $cert_data['subject']['CN']);
    if (in_array($commonname, $authorized_users)) {
        return true;
        //echo "$commonname je avtoriziran uporabnik, zato vidi trenutni čas: " . date("H:i");
    } else {
        return false;
        //echo "$commonname ni avtoriziran uporabnik in nima dostopa do ure";
    }

    //echo "<p>Vsebina certifikata: ";
    //var_dump($cert_data);
    }
}

