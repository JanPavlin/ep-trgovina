<!DOCTYPE html>
<?php 
                header('X-Frame-Options: DENY');
        header("X-XSS-Protection: 1; mode=block");
        header('X-Content-Type-Options: nosniff');
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("x-dns-prefetch-control: off");
        header("x-download-options: noopen");
        header("strict-transport-security: max-age=15552000; includeSubDomains");
        
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<html lang="en">

<head>
    <title>Web Shop</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<!--
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	-->
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<link rel="stylesheet" href="static/css/index.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
</head>
    <body>
        <div class="container">
            <div class="centered-text">
                <h1>Welcome to our Web Shop</h1>
            </div>
            <div class="centered-text">
                <h2 class="link"><a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "index.php"). "mainPages/notLoginUser/user.php")  ?>">CONTINUE</a> </h2>
            </div>
            
        </div>
	


    <footer class ="centered-text footer fixed-bottom">
        <a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "index.php"). "mainPages/client/client.php")  ?>">Client</a>
        <a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "index.php"). "mainPages/seller/seller.php"."?error=0" ) ?>">SELLER</a>
        <a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "index.php"). "mainPages/admin/admin.php")  ?>">ADMIN</a>
    </footer>

    </body>
</html>
