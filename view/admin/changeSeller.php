<?php 

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
   <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<!--
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	-->
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
    </head>
    <body>
        <?php //var_dump($sellerInfo)?>
        <h2>change seller data!!</h2>
          <?php if($error == 1){
       ?> <p style="color:red">Sprememba podatkov neuspesna</p> <?php    
         } else if($error == 2){
           ?> <p style="color:green">Sprememba uspesna.</p> <?php
       }
        ?>
        
    <form action="<?= BASE_URL . "updateSellerData" ?>" method="post">
        <input type="hidden" name="id" value="<?= $sellerInfo["id_oseba"] ?>"  />
        <p><label>Name: <input type="text" name="name" value="<?= $sellerInfo["ime"] ?>" autofocus required/></label></p>
        <p><label>Surname: <input type="text" name="surname" value="<?= $sellerInfo["priimek"] ?>" required/></label></p>
        <p><label>Email (na novo zmenjeno da bo username email): <input type="text" name="username" value="<?= $sellerInfo["username"] ?>" required/></label></p>
        <p><button>Change data</button></p>
    </form>
           
           <?php
            if($error == 6){
                ?> <p style="color:green">Sprememba statusa osebe</p> <?php
            }
           ?>
        
        <form action="<?= BASE_URL . "changeStatusSeller" ?>" method="post">
                <input type="hidden" name="id" value="<?php echo $sellerInfo['id_oseba']?>" />
                <input type="hidden" name="status" value="<?php echo $sellerInfo['id_statusosebe']?>" />
                <span><b>Status: </b> <?php if($sellerInfo['id_statusosebe'] == 2){
                                        ?>Active <?php
                                    }else if($sellerInfo['id_statusosebe'] == 3){
                                        ?>Inactive <?php
                                    }
                         
                ?> 
                </span>
                <button>Change status</button> 
        </form>
        
        
       <h2>Change your password??</h2>
        <?php if($error == 4){
       ?> <p style="color:red">Sprememba gesla neuspesna</p> <?php    
         } else if($error == 5){
           ?> <p style="color:green">Sprememba uspesna.</p> <?php
       }
        ?> 
    <form action="<?= BASE_URL . "changePasswordSeller" ?>" method="post">
        <input type="hidden" name="id" value="<?= $sellerInfo["id_oseba"] ?>"  />
        <p><label>New Pasword: <input type="password" name="passwordNew" value="" required/></label></p>
         <p><label>Confirm password: <input type="password" name="passwordConfirm" value="" required/></label></p>
        <p><button>Change password</button></p>
    </form>
        
       <a href="<?= htmlspecialchars(BASE_URL. "") ?>">BACK</a>
    </body>
</html>
