<?php 

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
       <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<!--
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	-->
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
    </head>
    <body>
        <h1>Welcome admin!!!!</h1>
    <form action="<?= BASE_URL . "logOut" ?>" method="post">
        <label>LogOut?? <input type="checkbox" name="logOut_confirm" /></label>
        <button type="submit">LogOut</button>
    </form>
        
        <h2>Uredi admin podatke</h2>
   <?php if($error == 1){
       ?> <p style="color:red">Sprememba podatkov neuspesna</p> <?php    
         } else if($error == 2){
           ?> <p style="color:green">Sprememba uspesna.</p> <?php
       }
   ?>
       <?php //var_dump($userInfo) ?>
    <form action="<?= BASE_URL . "updateAdminData" ?>" method="post">
        <input type="hidden" name="id" value="<?= $userInfo["id_oseba"] ?>"  />
        <p><label>Name: <input type="text" name="name" value="<?= $userInfo["ime"] ?>" autofocus required/></label></p>
        <p><label>Surname: <input type="text" name="surname" value="<?= $userInfo["priimek"] ?>" required/></label></p>
        <p><label>Email (na novo zmenjeno da bo username email): <input type="text" name="username" value="<?= $userInfo["username"] ?>" required/></label></p>
         
        <p><button>Change data</button></p>
    </form>
        
        
        <h2>Change your password??</h2>
        <?php if($error == 4){
       ?> <p style="color:red">Sprememba podatkov neuspesna</p> <?php    
         } else if($error == 5){
           ?> <p style="color:green">Sprememba uspesna.</p> <?php
       }
        ?>
    <form action="<?= BASE_URL . "changePassword" ?>" method="post">
        <input type="hidden" name="id" value="<?= $userInfo["id_oseba"] ?>"  />
        <p><label>Old Password: <input type="password" name="passwordOld" value="" required/></label></p>
        <p><label>New Pasword: <input type="password" name="passwordNew" value="" required/></label></p>
         <p><label>OldPassword: <input type="password" name="passwordConfirm" value="" required/></label></p>
        <p><button>Change password</button></p>
    </form>
        
        
        
    <h2>List of sellers</h2>
    
    <?php 
 foreach ($sellerData as $key => $seller) {
     //var_dump($seller);
     ?><form action="<?= BASE_URL . "editSeller" ?>" method="get">
                <input type="hidden" name="id" value="<?php echo $seller['id_oseba']?>" />
                <input type="hidden" name="error" value="3" />
                <span><b>Prodajalec: </b> <?php echo $seller['ime']." ".$seller['priimek']; ?> </span>
                <span><b>Status: </b> <?php if($seller['id_statusosebe'] == 2){
                                        ?>Active <?php
                                    }else if($seller['id_statusosebe'] == 3){
                                        ?>Inactive <?php
                                    } 
                ?> 
                </span>
                <button>Edit seller</button> 
        </form>
       
     <?php
 }
    ?>
        
    
    <a  class="btn btn-primary" href="<?=  htmlspecialchars(BASE_URL ."newSeller?error=0")  ?>">Create seller</a>
       <a  class="btn btn-primary" href="<?=  htmlspecialchars(BASE_URL ."getLog") ?>">SEE LOGS...</a>
    
    </body>
</html>
