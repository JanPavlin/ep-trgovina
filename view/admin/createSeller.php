<?php 

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
     <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<!--
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	-->
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
    </head>
    <body>
        <h1>Create new seller!!</h1>
           <?php if($error == 1){
       ?> <p style="color:yellow">Vsa polja morajo biti izpolnjena</p> <?php    
         } else if($error == 2){
           ?> <p style="color:red">Vneseni gesli se ne ujemata</p> <?php
       }else if($error == 3){
           ?> <p style="color:green">Uspesno vnesen nov seller.</p> <?php
       }
         ?>
        <form action="<?= BASE_URL . "newSeller" ?>" method="post">
        <p><label>Name: <input type="text" name="name" value="" autofocus required/></label></p>
        <p><label>Surname: <input type="text" name="surname" value="" required/></label></p>
        <p><label>Email (na novo zmenjeno da bo username email): <input type="text" name="username" value="" required/></label></p>
        <p><label>Status</label><select name="status">
            <option value="2">Active</option>
            <option value="3">Inactive</option>
        </select></p>
        <p><label>Password: <input type="password" name="password1" value="" required/></label></p>
        <p><label>Confirm password: <input type="password" name="password2" value="" required/></label></p>
        <p><button>Create seller</button></p>
    </form>
        
        <a href="<?= htmlspecialchars(BASE_URL. "") ?>">BACK</a>
    </body>
</html>
