<?php 
header('X-Frame-Options: DENY');
 header("X-XSS-Protection: 0");
 header('X-Content-Type-Options: nosniff');
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

    </head>
    <body>
        <?php
        // put your code here
        //var_dump($sellerData);
        echo "pozdravljen prodajalec";
        ?>
        
        <h1>Welcome <?php echo $sellerData['ime'] ?></h1>
    <form action="<?= BASE_URL . "logOut" ?>" method="post">
        <label>LogOut?? <input type="checkbox" name="logOut_confirm" /></label>
        <button type="submit">LogOut</button>
    </form>
        
        
         <?php if($error == 1){
       ?> <p style="color:red">Sprememba podatkov neuspesna</p> <?php    
         } else if($error == 2){
           ?> <p style="color:green">Sprememba uspesna.</p> <?php
       }
        ?>
        
        <form action="<?= BASE_URL . "changeSellerData" ?>" method="post">
        <input type="hidden" name="id" value="<?= $sellerData["id_oseba"] ?>"  />
        <p><label>Name: <input type="text" name="name" value="<?= $sellerData["ime"] ?>" autofocus required/></label></p>
        <p><label>Surname: <input type="text" name="surname" value="<?= $sellerData["priimek"] ?>" required/></label></p>
        <p><label>Email (na novo zmenjeno da bo username email): <input type="text" name="username" value="<?= $sellerData["username"] ?>" required/></label></p>
         
        <p><button>Change data</button></p>
    </form>
        
        
        <h2>Change your password??</h2>
        <?php if($error == 4){
       ?> <p style="color:red">Sprememba podatkov neuspesna</p> <?php    
         } else if($error == 5){
           ?> <p style="color:green">Sprememba uspesna.</p> <?php
       }
        ?>
    <form action="<?= BASE_URL . "changeSellerPassword" ?>" method="post">
        <input type="hidden" name="id" value="<?= $sellerData["id_oseba"] ?>"  />
        <p><label>Old Password: <input type="password" name="passwordOld" value="" required/></label></p>
        <p><label>New Pasword: <input type="password" name="passwordNew" value="" required/></label></p>
         <p><label>OldPassword: <input type="password" name="passwordConfirm" value="" required/></label></p>
        <p><button>Change password</button></p>
    </form>
        
        
        
        
        
        <?php 
        //var_dump(BASE_URL);
        //var_dump($allProducts);
        ?>
        <div class="container-fluid">
            <div class="row">
                <?php 
                        foreach ($allProducts as $key => $product) {
                            
                            ?> 
                                  <div class="col-sm-3">
                                    <div class="card">
                                           <?php if($product['slika']){
                                            ?>  <img  src="<?php echo IMAGES_URL.$product['slika']?>" alt="Card image cap"><?php
                                        }else{
                                            ?><img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%282%29.jpg" alt="Card image cap"> <?php
                                        }
                                        
                                        ?>
                                        <div class="card-body">
                                            <h4 class="card-title"> <?php echo $product['ime_izdelka'] ?></h4>
                                            <p class="card-text"> <?php echo $product['opis_izdelka'] ?></p>
                                            <div class = "row">
                                                <div class="col-sm-4">
                                                    <a href="<?= BASE_URL ."eidtProduct?productID=".$product['id_izdelka']."&error=0" ?>" class="btn btn-primary">EDIT</a>
                                                </div>
                                                <div class="col-sm-4">
                                                    <b>Status: </b>: 
                                                            <?php if($product['id_statusaizdelka'] == 1){
                                                                ?> ACTIVE<?php
                                                            }else{
                                                                ?> INACTIVE<?php
                                                            }
                                                    ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <b>Cena: </b> <?php echo $product['postavka'] ?> EUR
                                                    <?php 
                                                        if($product['score'] == 0.0){
                                                            ?> <p>No score yet</p><?php
                                                        }else{
                                                            ?><p>Score: <?php echo $product['score'] ?></p><?php
                                                        }
                                                    ?>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                
                            <?php
                            
                        }
                ?>
            </div>
                       
        </div>
        <h2>NEW PRODUCT</h2>
            
         <a class="btn btn-primary" href="<?= BASE_URL ."newProduct?error=0" ?>">Add new product to the store..</a>
        
        <h2> All customers!!!!</h2>

        
        
            <?php 
            foreach ($allCustomers as $key => $customer) {
                //var_dump($customer);
                ?><form action="<?= BASE_URL . "editCustomer" ?>" method="get">
                           <input type="hidden" name="id" value="<?php echo $customer['id_oseba']?>" />
                           <input type="hidden" name="error" value="0" />
                           <span><b>Stranka: </b> <?php echo $customer['ime']." ".$customer['priimek']; ?> </span>
                           <span><b>Status: </b> <?php if($customer['id_statusosebe'] == 4){
                                                   ?>Active <?php
                                               }else if($customer['id_statusosebe'] == 5){
                                                   ?>Inactive <?php
                                               } 
                           ?> 
                           </span>
                           <button>Edit customer</button> 
                   </form>

                <?php
            }
    ?>
        
        <h2> list  čakajočih/neobdelanih računov/možnost da se prekliče</h2>
        
           <?php 
              //var_dump($invoices);
                foreach ($oddanaNarocila as $key => $invoice) {
                    //var_dump($seller);
                    ?><form action="<?= BASE_URL . "editOrder" ?>" method="get">
                               <input type="hidden" name="id" value="<?php echo $invoice['id_narocilo']?>" />
                               <span><b>ID: </b> <?php echo $invoice['id_narocilo']; ?> </span>
                               <span>TOTAL: <?php echo $invoice['skupajPlacilo']?></span>
                               <button>View order</button> 
                       </form>

                    <?php
         }
    ?>
        
        
        
        <h2> list  potrjenih računov:</h2>
        
               <?php 
              //var_dump($invoices);
                foreach ($potrjenaNarocila as $key => $invoice) {
                    //var_dump($seller);
                    ?><form action="<?= BASE_URL . "editInvoice" ?>" method="get">
                               <input type="hidden" name="id" value="<?php echo $invoice['id_narocilo']?>" />
                               <span><b>ID: </b> <?php echo $invoice['id_narocilo']; ?> </span>
                               <span>TOTAL: <?php echo $invoice['skupajPlacilo']?></span>
                               <button>View invoice</button> 
                       </form>

                    <?php
         }
        ?>
        
        
        <h2> list  storniranih naročil</h2>
        
            <?php 
              //var_dump($invoices);
                foreach ($storniranaNarocila as $key => $invoice) {
                    //var_dump($seller);
                    ?><form action="<?= BASE_URL . "showInvoice" ?>" method="get">
                               <input type="hidden" name="id" value="<?php echo $invoice['id_narocilo']?>" />
                               <span><b>ID: </b> <?php echo $invoice['id_narocilo']; ?> </span>
                               <span>TOTAL: <?php echo $invoice['skupajPlacilo']?></span>
                               <button>Show canceled invoice</button> 
                       </form>

                    <?php
         }
        ?>
        
        
         
       <a href="<?= BASE_URL ."newClient?"."&error=0" ?>" class="btn btn-primary">new client</a>
    </body>
</html>
