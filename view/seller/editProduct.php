<?php 
header('X-Frame-Options: DENY');
 header("X-XSS-Protection: 0");
 header('X-Content-Type-Options: nosniff');
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
       <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<!--
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	-->
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
    </head>
    <body>
        <h1>EDIT PRODUCT</h1>
        <?php
        // put your code here
       // var_dump($product);
        //var_dump($error);
        ?>
        
            <?php if($error == 1){
       ?> <p style="color:red">Cant' change data</p> <?php    
         } else if($error == 2){
           ?> <p style="color:green">Success!!</p> <?php
       }
        ?>
        
        <form action="<?= BASE_URL . "updateProduct" ?>" method="post">
        <input type="hidden" name="id" value="<?php echo $product['id_izdelka']?>" />
        <p><label>Product Name: <input type="text" name="product" value="<?=$product['ime_izdelka'] ?>" autofocus required/></label></p>
        <p><label>Description: <textarea  name="description" required/><?= $product['opis_izdelka'] ?> </textarea></label></p>
        
        <p><label>Price:  <input type="number" name="price" min="1" max="100" value="<?= $product['postavka'] ?>" required>EUR</label></p>
        
        <p><button>Change product</button></p>
        
        </form>
           
           
               <?php
            if($error == 6){
                ?> <p style="color:green">Sprememba statusa osebe</p> <?php
            }
           ?>
        
        <form action="<?= BASE_URL . "prductStatusChange" ?>" method="post">
                <input type="hidden" name="id" value="<?php echo $product['id_izdelka']?>" />
                <input type="hidden" name="status" value="<?php echo $product['id_statusaizdelka']?>" />
                <span><b>Status: </b> <?php if($product['id_statusaizdelka'] == 1){
                                        ?>Active <?php
                                    }else if($product['id_statusaizdelka'] == 2){
                                        ?>Inactive <?php
                                    }
                         
                ?> 
                </span>
                <button>Change status</button> 
        </form>
                
                <div class="container-fluid">
                    <div class="row">
                              <?php
                            //var_dump($productPicures);
                            foreach ($productPicures as $key=> $picture){

                                ?> 
                                    <div class="col-sm-3">
                                         <img  src="<?php echo IMAGES_URL.$picture['potSlike'] ?>" alt="Card image cap"> 
                                        <form method='POST' action='<?= BASE_URL . "deletePicture" ?>' >
                                             <input type="hidden" name="productId" value="<?php echo $product['id_izdelka']?>" />
                                            <input type="hidden" name="id" value="<?php echo $picture['id_slike']?>" />
                                            <input type="hidden" name="picturePath" value="<?php echo $picture['potSlike']?>" />
                                            <input type='submit' value="DELETE">
                                     </form>

                                    </div>
                                <?php
                            }

                            ?>
                        
                    </div>
                    
                </div>
  
                
        <h2>ADD IMAGES</h2>
        
             <?php if($error == 10){
       ?> <p style="color:red">no file selected</p> <?php    
         } else if($error == 11){
           ?> <p style="color:red">Upload failed</p> <?php
       }
       else if($error == 12){
           ?> <p style="color:green">Img added</p> <?php
       }
        ?>
        <form method='POST' action='<?= BASE_URL . "addPicture" ?>' enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $product['id_izdelka']?>" />
            <input type='file' name='UploadImage'>
            <input type='submit' value="submit">
        </form>
        
           
        <form action="<?= BASE_URL . "deleteProduct" ?>" method="post">
                <input type="hidden" name="id" value="<?php echo $product['id_izdelka']?>" />
                 <label>Delete? <input type="checkbox" name="deleteConfirm" /></label>
                <button>Delete product</button> 
        </form>
        
         <a href="<?= htmlspecialchars(BASE_URL. "sellerPage"."?error=0") ?>">BACK</a>
    </body>
</html>
