<?php 
header('X-Frame-Options: DENY');
 header("X-XSS-Protection: 0");
 header('X-Content-Type-Options: nosniff');
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<!--
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	-->
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
    </head>
    <body>
        <?php //var_dump($customerMore);
              //var_dump($customerBasic)
        ?>
        <h2>change client data!!</h2>
          <?php if($error == 1){
       ?> <p style="color:red">Sprememba podatkov neuspesna, napacen format podatkov...</p> <?php    
         } else if($error == 2){
           ?> <p style="color:green">Sprememba uspesna.</p> <?php
       }else if($error == 7){
           ?> <p style="color:red">Napacen format tel. stevilke.. ali predolg naslov za zapisat v bazo.</p> <?php
       }
       else if($error == 8){
           ?> <p style="color:red">Predolg naslov.</p> <?php
       }
        ?>
        
    <form action="<?= BASE_URL . "updateCustomerData" ?>" method="post">
        <input type="hidden" name="id" value="<?= $customerBasic["id_oseba"] ?>"  />
        <p><label>Name: <input type="text" name="name" value="<?= $customerBasic["ime"] ?>" autofocus required/></label></p>
        <p><label>Surname: <input type="text" name="surname" value="<?= $customerBasic["priimek"] ?>" required/></label></p>
        <p><label>Email (na novo zmenjeno da bo username email): <input type="email" name="username" value="<?= $customerBasic["username"] ?>" required/></label></p>
        <p><label>Postal code: <input type="text" name="Post" value="<?= $customerMore["posta"] ?>" required/></label></p>
        <p><label>City: <input type="text" name="City" value="<?= $customerMore["kraj"] ?>" required/></label></p>
        <p><label>Street: <input type="text" name="Street" value="<?= $customerMore["ulica"] ?>" required/></label></p>
        <p><label>Street number: <input type="text" name="Streetnumber" value="<?= $customerMore["stevilka"] ?>" required/></label></p>
        <p><label>TelNumber: <input type="tel" name="telephoneNumber" value="<?= $customerMore["telefonska"] ?>" required/></label></p>
        <p><button>Change Customer Data</button></p>
    </form>
           
           <?php
        //var_dump(BASE_URL);
        if(BASE_URL != "/netbeans/ep-trgovina/mainPages/client/client.php/"){
            if($error == 6){
                ?> <p style="color:green">Sprememba statusa osebe</p> <?php
            }
            
             ?>  <form action="<?= BASE_URL . "changeStatusCustomer" ?>" method="post">
                <input type="hidden" name="id" value="<?php echo $customerBasic['id_oseba']?>" />
                <input type="hidden" name="status" value="<?php echo $customerBasic['id_statusosebe']?>" />
                <span><b>Status: </b> <?php if($customerBasic['id_statusosebe'] == 4){
                                        ?>Active <?php
                                    }else if($customerBasic['id_statusosebe'] == 5){
                                        ?>Inactive <?php
                                    }
                         
                ?> 
                </span>
                <button>Change status</button> 
            </form>
            <?php
        }
            
            
           ?>
        
     
        
        
       <h2>Change your password??</h2>
        <?php if($error == 4){
       ?> <p style="color:red">Sprememba gesla neuspesna</p> <?php    
         } else if($error == 5){
           ?> <p style="color:green">Sprememba uspesna.</p> <?php
       }
        ?> 
    <form action="<?= BASE_URL . "changePasswordCustomer" ?>" method="post">
        <input type="hidden" name="id" value="<?= $customerBasic["id_oseba"] ?>"  />
        <p><label>New Pasword: <input type="password" name="passwordNew" value="" required/></label></p>
         <p><label>Confirm password: <input type="password" name="passwordConfirm" value="" required/></label></p>
        <p><button>Change password</button></p>
    </form>
        
        <?php 
            if(BASE_URL == "/netbeans/ep-trgovina/mainPages/client/client.php/"){
                ?> <a href="<?= htmlspecialchars(BASE_URL. "clientMain") ?>">BACK</a><?php
            }else{
                ?> <a href="<?= htmlspecialchars(BASE_URL. "") ?>">BACK</a><?php
            }
        
        ?>
       
    </body>
</html>
