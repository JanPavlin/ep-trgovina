<?php 
header('X-Frame-Options: DENY');
 header("X-XSS-Protection: 0");
 header('X-Content-Type-Options: nosniff');
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <h1> Confirmed order</h1>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                 INVOICE confirmed.
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            
                            <td>
                                Ime: <?php echo $client['ime']?>.<br>
                                Priimek: <?php echo $client['priimek']?><br>
                                <?php echo $client['username']?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            
            <tr class="heading">
                <td>
                    Item
                </td>
                 <td>
                    Quantity
                </td>
                <td>
                    Price EUR
                </td>
            </tr>
            
            <?php 
                foreach($all as $key=> $product){
                    ?> 
            
                        <tr class="item" >
                            <td>
                                <?php echo $product['ime_izdelka'] ?>
                            </td>

                            <td>
                                <?php echo $product['kolicina_izdelka'] ?>
                            </td>

                            <td style="float:right">
                                <?php echo $product['postavka'] ?> 
                            </td>
                        </tr>
                        
                        <?php
                }
                
            ?>
         
            <tr class="total">
                <td></td>
                
                <td>
                   Total: <?php echo $invoice['skupajPlacilo'];?> EUR
                </td>
            </tr>
        </table>
    </div>
    
        <br>
        <a href="<?= htmlspecialchars(BASE_URL)."cancelOrder?invoiceId=".$invoice['id_narocilo']  ?>">Cancel Order</a>
        <br>
        <a href="<?= htmlspecialchars(BASE_URL)."sellerPage?error=0"  ?>">BACK</a>
  
</body>
</html>
