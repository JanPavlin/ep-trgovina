<?php 
header('X-Frame-Options: DENY');
 header("X-XSS-Protection: 0");
 header('X-Content-Type-Options: nosniff');
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
     <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<!--
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	-->
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
    </head>
    <body>
          <h1>Add new product to store!!</h1>
           <?php if($error == 1){
       ?> <p style="color:red">Description can not be empty</p> <?php    
         } else if($error == 2){
           ?> <p style="color:green">Product addet to database</p> <?php
       }
        ?>
        
        <form action="<?= BASE_URL . "newProduct" ?>" method="post">
        <p><label>Product Name: <input type="text" name="product" value="" autofocus required/></label></p>
        <p><label>Description: <textarea  name="description" value="" required/> </textarea></label></p>
        
        <p><label>Price:  <input type="number" name="price" min="1" max="100" value="1" required>EUR</label></p>
        <p><label>Status</label><select name="status">
            <option value="1">Active</option>
            <option value="2">Inactive</option>
        </select></p>
        
        <p><button>Add product</button></p>
        
    </form>
        
        <a href="<?= htmlspecialchars(BASE_URL. "sellerPage"."?error=0") ?>">BACK</a>
    </body>
</html>
