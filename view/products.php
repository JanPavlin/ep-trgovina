<!DOCTYPE html>
<?php 

?>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

	
</head>

<body>
    <h1> Check out our products</h1>
        

	<div class="container-fluid">
	    <div class="row">
                        <?php
                        foreach ($allProducts as $key => $product) {
                            ?> 
                            <div class="col-sm-3">
                                <div class="card">
                                  <?php if($product['slika']){
                                            ?>  <img  src="<?php echo IMAGES_URL.$product['slika']?>" alt="Card image cap"><?php
                                        }else{
                                            ?><img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%282%29.jpg" alt="Card image cap"> <?php
                                        }
                                        
                                        ?>

                                    <div class="card-body">
                                        <h4 class="card-title"> <?php echo $product['ime_izdelka'] ?></h4>
                                        <p class="card-text"> <?php echo $product['opis_izdelka'] ?></p>
                                        <div class = "row">

                                            <div class="col-sm-6">
                                                <a href="<?= BASE_URL . "viewProduct?productID=" . $product['id_izdelka'] ?>" class="btn btn-outline-info">Info</a>
                                            </div>

                                         
                                        </div>
                                        <div class = "row">
                                            <div class="col-sm-12">
                                                <p class="text-primary">Cena:  <?php echo $product['postavka'] ?> EUR
                                                <p>RATING: <?php 
                                                    if($product['score'] == 0.0){
                                                        ?> No score yet. <?php
                                                    }else{
                                                        echo $product['score'] ?> od 10 <?php
                                                    }
                                                 ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <?php
                        }
                        ?>
                    </div>
  
           


	</div>
        
         <a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "user.php"). "../../")  ?>">BACK</a>
         <br>
         <a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "user.php"). "../../mainPages/client/client.php/register"."?error=0")  ?>">REGISTER</a>

</body>

<script>
    
    /*var products = <?php echo json_encode($products); ?>;
    
    function redirect(id){ 
         <?php $a = BASE_URL."viewProduct?id=" ;   ?>;
        n =  <?php echo json_encode($a); ?>;
       
        return (n + id )
                
       }
       
    function makePath(path){
         <?php $a = IMAGES_URL;   ?>;
        n =  <?php echo json_encode($a); ?>;
        console.log(n);
        return (n + path );
    }*/
       
</script>
<!--<script src="<?= JS_URL . "products.js" ?>"></script>-->
</html>
