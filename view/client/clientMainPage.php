<?php 

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

        <style>

            .modal-dialog {
                min-width: 50%;
            }

            .modal-content {
                height: auto;
                min-height: 100%;
                border-radius: 0;
                overflow-y: scroll;
            }

            .basket.modal-dialog {
                min-width: 95%;
            }

            .basket.modal-content {
                height: auto;
                min-height: 100%;
                border-radius: 0;
                overflow-y: scroll;
            }



        </style>


    </head>
    <body>
        <!--
        <div id="exTab3" class="container">	
            <ul  class="nav nav-pills">
                                    <li class="active">
                                        <a  href="#1b" data-toggle="tab">Overview</a>
                                    </li>
                                    <li>
                                        <a href="#2b" data-toggle="tab">Using nav-pills</a>
                                    </li>
                                    <li><a href="#3b" data-toggle="tab">Applying clearfix</a>
                                    </li>
                            <li><a href="#4a" data-toggle="tab">Background color</a>
                                    </li>
                            </ul>
        -->



        <!-- Navbar START -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Welcome, <?php echo $client['ime'] ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation" style="">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarColor03">
                <ul class="nav nav-pills mr-auto">
                    <li>
                        <a class="nav-link active" href="#allTab" data-toggle="tab">All products</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#ordersTab" data-toggle="tab">Past purchases</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#settingsTab" data-toggle="tab">Settings</a>
                    </li>
                </ul>

                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" placeholder="Search" type="text">
                    <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
                </form>
                <div></div>
                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#basketModal">View basket</button>
                <div></div>
                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#logoutModal">Logout</button>

                <!-- 
               <form action="<?= BASE_URL . "clientLogOut" ?>" method="post">
                   <label>LogOut?? <input type="checkbox" name="logOut_confirm" /></label>
                   <button type="submit">LogOut</button>
               </form>
                -->
            </div>
        </nav>

        <!-- Navbar END -->

       

        <!-- Modals START -->
        <div id="basketModal" class="modal fade">
            <div class="basket modal-dialog modal-lg">
                <div class="basket modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Basket</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    </div>
                    <div class="modal-body">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Amount</th>
                                    <th>Price</th>
                                    <th></th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                foreach ($cart as $key => $product) {
                                    //var_dump($product['quantity']);
                                    ?> 

                                    <tr>
                                        <td><?php echo $product['ime_izdelka'] ?></td>
                                        <td>
                                            <form class="form-inline" action="<?= BASE_URL . "updateCart" ?>" method="post">
                                                <input type="hidden" name="id" value="<?php echo $product['id_izdelka'] ?>"/>
                                                <div class = "form-group mx-sm-2">
                                                    <input class="form-control" type="number" min="1"  value="<?php echo $product['quantity'] ?>" id="amount" name="amount">
                                                </div>

                                                <div class="form-group mx-sm-2">

                                                    <button type="submit" class="btn btn-outline-secondary">UPDATE</button>
                                                </div>
                                            </form>

                                        </td>
                                        <td><?php echo $product['postavka'] ?> EUR</td>
                                        <td>
                                            <form action="<?= BASE_URL . "deleteFromBasket" ?>" method="post">
                                                <input type="hidden" name="id" value="<?php echo $product['id_izdelka'] ?>"/>
                                                <button type="submit" class="btn btn-outline-secondary">x</button>
                                            </form>
                                        </td>
                                    </tr>


                                    <?php
                                }
                                ?>


                            </tbody>
                        </table>
                    </div>

                    <div class="modal-footer">
                        <div class="pull-left">
                            <h4>Total price: <?php echo $total ?> </h4>
                        </div>
                        <form action="<?= BASE_URL . "purgeCart" ?>" method="post">
                            <button type="submit" class="btn btn-outline-danger">Empty cart</button>
                        </form>
                        <a href="<?= BASE_URL . "checkout" ?>" class="btn btn-primary">Checkout</a>

                    </div>


                </div>
            </div>
        </div>

        <div id="logoutModal" class="modal fade">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Are you sure?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer">

                        <form action="<?= BASE_URL . "clientLogOut" ?>" method="post">
                            <label style="display: none;">LogOut?? <input type="checkbox" name="logOut_confirm" checked></label>
                            <button type="submit" class="btn btn-danger">Log me out!</button>
                        </form>

                        <button type="button" class="btn btn-primary" data-dismiss="modal">I've changed my mind</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modals END -->


        <div class="container-fluid">
            <!-- Zavihki START -->
            <div class="tab-content">
                <!-- Zavihek 1 START -->
                <div class="tab-pane active" id="allTab">
                    <div class="row">
                        <?php
                        foreach ($allProducts as $key => $product) {
                            ?> 
                            <div class="col-sm-3">
                                <div class="card">
                                  <?php if($product['slika']){
                                            ?>  <img  src="<?php echo IMAGES_URL.$product['slika']?>" alt="Card image cap"><?php
                                        }else{
                                            ?><img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%282%29.jpg" alt="Card image cap"> <?php
                                        }
                                        
                                        ?>

                                    <div class="card-body">
                                        <h4 class="card-title"> <?php echo $product['ime_izdelka'] ?></h4>
                                        <p class="card-text"> <?php echo $product['opis_izdelka'] ?></p>
                                        <div class = "row">

                                            <div class="col-sm-6">
                                                <a href="<?= BASE_URL . "detailsAndScore?productID=" . $product['id_izdelka'] ?>" class="btn btn-outline-info">Info</a>
                                            </div>

                                            <div class="col-sm-6">
                                                <form action="<?= BASE_URL . "basketAdd" ?>" method="post">
                                                    <input type="hidden" name="id" value="<?php echo $product['id_izdelka'] ?>"/>
                                                    <button type="submit" class="btn btn-outline-primary">Basket</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class = "row">
                                            <div class="col-sm-12">
                                                <p class="text-primary">Cena:  <?php echo $product['postavka'] ?> EUR
                                                <p>RATING: <?php 
                                                    if($product['score'] == 0.0){
                                                        ?> No score yet. <?php
                                                    }else{
                                                        echo $product['score'] ?> od 10 <?php
                                                    }
                                                 ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <?php
                        }
                        ?>
                    </div>



                </div>
                
                
                
                <!-- Zavihek 1 END -->
                <!-- Zavihek 2 START -->
                <div class="tab-pane" id="ordersTab">
                    <div class="row">
                        <?php
                        //var_dump($invoices);
                        foreach ($invoices as $key => $invoice) {
                            ?>
                            <div class="col-sm-6">

                                <?php
                                if ($invoice['id_statusnarocila'] == 1) {
                                ?>
                                    <div class="card border-primary mb-3">
                                
                                <?php
                                } else if ($invoice['id_statusnarocila'] == 2) {
                                ?>    
                                    <div class="card border-success mb-3">
                                
                                <?php
                                } else {
                                ?>  
                                    <div class="card border-danger mb-3">
                                
                                <?php
                                }
                                ?> 
<!--
                                <div class="card border-primary mb-3">
-->
                                    <!--<div class="card-header">Header</div>-->
                                    <div class="card-body text-primary">

                                        <form action="<?= BASE_URL . "showOneInvoice" ?>" method="get">
                                            <input type="hidden" name="id" value="<?php echo $invoice['id_narocilo'] ?>" />
                                            <h4 class="card-title">Order #<?php echo $invoice['id_narocilo']; ?></h4>
                                            <b>Customer ID: </b> <?php echo $invoice['id_oseba']; ?> 
                                            <div>
                                            </div>
                                            <b>Status: </b> <?php if ($invoice['id_statusnarocila'] == 1) {
                                    ?> Order awarded<?php
                                            } else if ($invoice['id_statusnarocila'] == 2) {
                                                ?>Order confirmed <?php
                                            } else {
                                                ?>Order canceled <?php
                                            }
                                            ?> 
                                            <div>

                                                Total: <?php echo $invoice['skupajPlacilo'] ?> EUR
                                            </div>
                                            <div> </div>
                                            <button class="btn btn-outline-primary">View order</button> 
                                        </form>

                                    </div>
                                </div>

                            </div>
                            <?php
                        }
                        ?>
                    </div>


                </div>
                <!-- Zavihek 2 END -->
                <!-- Zavihek 3 START -->
                <div class="tab-pane" id="settingsTab">
                    <a href="<?= htmlspecialchars(BASE_URL . "editCustomer?id=" . $client['id_oseba'] . "&error=0") ?>">EDITDATA</a>
                    <br>
                    <br>
                    <!--<a href="<?= htmlspecialchars(BASE_URL . "deleteAcc?id=" . $client['id_oseba']) ?>">DELETE ACCOUNT</a>-->
                </div>
                <!-- Zavihek 3 END -->



            </div>
            <!-- Zavihki END -->
        </div>    

    </body>

    <script>
        /*
         $(document).ready(function(){
         var products = <?php echo json_encode($cart); ?>;
         console.log(products);
         
         if(products.length > 0){
         $("#basketModal").modal('show');
         }
         
         
         });
         */


    </script>
</html>
