<?php 

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<!--
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	-->
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
    </head>
    <body>
        <?php
        // put your code here
        
        //var_dump($product);
        ?>
        <h1> PRODUCT DETAIL: </h1>
        <p><?php echo $product['ime_izdelka']  ?> </p>
        <p><?php echo $product['opis_izdelka']  ?> </p>
        <p>postavka:  <?php echo $product['postavka']  ?> </p>
       
        <?php 
        foreach ($productPicures as $key=> $picture){
            ?><img  src="<?php echo IMAGES_URL.$picture['potSlike'] ?>" alt="Card image cap"> <?php
        }
        
        ?>
        
        ?>
        
        <br>
        
        
       
        
        <h2>PRODUCT RATING: <?php 
            if($score == 0.0){
                ?><span> No score yet</span> <?php
            }else{
                ?><span> <?php echo $score ?> </span> od 10 <?php
            }
        ?></h2>
        <?php 
            if($canScore){
                ?> 
                <form action="<?= BASE_URL . "scoreProduct" ?>" method="post">
                    <input type="hidden" name="id" value="<?php echo $product['id_izdelka']?>" />
                    <p><label>Oceni (1-10):  <input type="number" name="score" min="1" max="10" value="8" required> </label><span><button>Rate product</button></span></p>
                </form>

                    
                    <?php
            }else{
                ?> <p> You have already score this product</p><?php
            }
        
        ?>

        
        <br>
        <a href="<?= htmlspecialchars(BASE_URL)  ?>">BACK</a>
        
    </body>
</html>
