
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<!--
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	-->
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
</head>

<body>
 
	<div class="container login">

		<div class="row login-window">
			<div class="col text-center" id="instructions">
				<h2>Register</h2>
				<p>All fields are required</p>
			</div>
		</div>
            
              <?php if($error == 1){
       ?> <p style="color:red">Paswords do not match</p> <?php    
         } else if($error == 2){
           ?> <p style="color:green">Registracija uspešna. poslali smo vam aktivacijski link na mail.</p> <?php
       }else if($error == 3){
           ?> <p style="color:red">Nepravilen format podatkov</p> <?php
       }else if($error == 6){
           ?> <p style="color:red">YOU ARE A ROBOT</p> <?php
       }else if($error == 7){
           ?> <p style="color:red">Client already exist</p> <?php
       }
    ?>
        <section class="login-form">
            <form action="" method="post" action="<?= BASE_URL . "register" ?>">
                <div class="form-group">
                    <input type="email" class="form-control" id="email" placeholder="Email" name="email" required>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="pass" placeholder="Password" name="password" required>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="pass-repeat" placeholder="Repeat your password" name="confirm-password" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="first-name" placeholder="First name" name="customer-first-name" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="last-name" placeholder="Last name" name="customer-last-name" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="address-line-1" placeholder="Ljubljana" name="kraj" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="address-line-1" placeholder="Cankarjeva ulica" name="ulica" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="address-line-1" placeholder="Hisna st: 38a" name="stevilka" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="address-line-1" placeholder="posta: 8000" name="posta" required>
                </div>
                <div class="form-group">
                    <input type="tel" class="form-control" id="address-zip" placeholder="041257942" name="telephone-number" required>
                </div>           
                
                <div class="g-recaptcha" data-sitekey="6LfGdj4UAAAAAD9QPZdNNnnjiaIkGhOTvGra5476"></div>
                <button type="submit" class="btn btn-primary">Register</button>
                <div id="register-invitation">Already a customer? <a href="<?= BASE_URL."login?error=0" ?>">Sign in now!</a></div>
            </form>
			<div id="alert"></div>

		</section>
	</div>
        
        <a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "client.php"). "../../")  ?>">BACK</a>

</body>

<script src='https://www.google.com/recaptcha/api.js'></script>
</html>
