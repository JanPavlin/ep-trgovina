
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	
	<!--
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	-->
	
	<link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
</head>

<body>
 
	<div class="container login">

		<div class="row login-window">
			<div class="col text-center" id="instructions">
				<h2>Sign In</h2>
				<p>Please sign-in</p>
			</div>
		</div>
            
                           <?php if($error == 1){
       ?> <p style="color:red">Paswords do not match</p> <?php    
         }else if($error == 2){
            ?> <p style="color:red">Inactive user</p> <?php   
         }else if($error == 3){
            ?> <p style="color:red">wrong input fomrat. username shoud be email</p> <?php   
         }
    ?>

		<section class="login-form">
			<form action="<?= BASE_URL . "login" ?>" method="post">
				<div class="form-group">
                                    <input type="text" class="form-control" id="email" placeholder="Username" name="username" required>
				</div>
				<div class="form-group">
                                    <input type="password" class="form-control" id="geslo" placeholder="Password" name="password" required>
				</div>
				<button type="submit" class="btn btn-primary">Sign in</button>
				<div class="g-signin2"></div>


			</form>

			<div class="bottom-text">Not a customer yet? <a href="<?= BASE_URL."register?error=0" ?>">Register now!</a></div>
			<div id="alert"></div>

		</section>
	</div>
         <a href="<?= htmlspecialchars(rtrim($_SERVER["SCRIPT_NAME"], "client.php"). "../../")  ?>">BACK</a>
</body>

</html>
