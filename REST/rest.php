<?php

// enables sessions for the entire app
session_start();

require_once("restController.php");
require_once("../mainPages/ViewHelper.php");
require_once ("../resources/database/database.php");
require_once("../controllers/sessionCart.php");

define("BASE_URL", rtrim($_SERVER["SCRIPT_NAME"], "rest.php"));
define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "rest.php") . "static/images/");
define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "rest.php") . "static/css/");

$path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";
    header('X-Frame-Options: DENY');
 header("X-XSS-Protection: 0");
 header('X-Content-Type-Options: nosniff');
$urls = [
    # REST API
    "/^api\/products\/(\d+)$/" => function ($method, $id = null) {
        // TODO: izbris knjige z uporabo HTTP metode DELETE
        switch ($method) {
            case "GET":
                REST::getOneProduct($id);
                break;

            default: 
                ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
                break;
        }
    },
    "/^api\/products$/" => function ($method, $id = null) {
        //var_dump($method);
        switch ($method) {
            case "GET":
                REST::restAllProducts();
                break;
            default:
                ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
                break;
        }
    },
    "/^api\/client\/?(\d+)?$/" => function ($method, $id = null) {
        //var_dump($method);
        switch ($method) {
            case "POST":
                //BooksRESTController::add();
                //echo ViewHelper::renderJSON("CliNET", 202);
                REST::restLogIn();
                break;
            case "GET":
                if($id != null){
                    REST::getUserData($id);
                }else{
                    ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
                }
                
                break;
            case "PUT":
                //var_dump("puttttt");
                if($id != null){
                    REST::changeUserData($id);
                }else{
                    ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
                }
                
                break;
            default:
                ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
                break;
        }
    },
        "/^api\/clientPassword\/(\d+)$/" => function ($method, $id = null) {
        //var_dump($method);
        switch ($method) {
            case "PUT":
                //var_dump("puttttt");
                if($id != null){
                    REST::changeUserPassword($id);
                }else{
                    ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
                }
                break;
            default:
                ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
                break;
        }
    },
    
    "/^api\/cartProducts\/?(\d+)?$/" => function ($method, $id = null) {
        // TODO: izbris knjige z uporabo HTTP metode DELETE
        //var_dump($method);
        switch ($method) {
            case "POST":
                REST::addToCart();
                break;
            case "PUT":
                REST::updateInCart();
                break;
            case "DELETE":
                if($id != null){
                    REST::deleteInCart($id);
                }else{
                    ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
                }
                break;
            default: 
                ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
                break;
        }
    },
            
    "/^api\/cart$/" => function ($method, $id = null) {
        // TODO: izbris knjige z uporabo HTTP metode DELETE
        switch ($method) {
            case "DELETE":
                REST::purgeCart();
                break;
            case "GET":
                REST::getCart();
                break;
            case "POST":
                REST::purchase();
                break;
            default: 
                ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
                break;
        }
    },
            
      "/^api\/invoices\/(\d+)\/?(\d+)?$/" => function ($method, $idClient = null, $idInvoice = null) {
        // TODO: izbris knjige z uporabo HTTP metode DELETE
        //var_dump($method);
        if($method == "GET"){
            if($idInvoice == null){
                //pridobi vsa narocila
                //var_dump($idClient);
                REST::getOsebaAllOrders($idClient);
            }else{
                //pridobi specificno narocilo...
                //var_dump($idInvoice);
                REST::getOsebaSpecificOrder($idInvoice);
            }
        }else{
             ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
        }
        
    },
];

foreach ($urls as $pattern => $controller) {
    if (preg_match($pattern, $path, $params)) {
        try {
            $params[0] = $_SERVER["REQUEST_METHOD"];
            //var_dump($params);
            $controller(...$params);
        } catch (InvalidArgumentException $e) {
            ViewHelper::error404();
        } catch (Exception $e) {
            ViewHelper::displayError($e, true);
        }

        exit();
    }
}

ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
