<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class REST{
    public static function restAllProducts(){
        //var_dump(DB::getIzdelekActive());
        echo ViewHelper::renderJSON(DB::getIzdelekActive());
    }
    
    public static function getOneProduct($id){
        //var_dump(DB::getOneProduct($id));
        echo ViewHelper::renderJSON(DB::getOneProduct($id));
    }
    public static function restLogIn(){
          $rules = [
            "username" => FILTER_VALIDATE_EMAIL,
            "password" => FILTER_SANITIZE_SPECIAL_CHARS
        ];
          
        
        //$entityBody = file_get_contents('php://input');
        //echo ViewHelper::renderJSON("Raw input: ");
        //echo ViewHelper::renderJSON($entityBody);
          
        $_POST = [];
        parse_str(file_get_contents("php://input"), $_POST);
        
        
        
        //$json = file_get_contents('php://input');
        //$obj = json_decode($json, true);
        
        //echo ViewHelper::renderJSON($obj);
        $data = filter_var_array($_POST, $rules);
        
        
        
        //$data = filter_input_array(INPUT_POST, $rules);
        //var_dump(INPUT_POST);
        //echo ViewHelper::renderJSON("This is data: ");
        //echo ViewHelper::renderJSON($data);
        
        if(self::checkValues($data)){
            $arr = DB::login($data['username']);
           //echo ViewHelper::renderJSON($arr['id_oseba']);
            if ($arr){
                   //var_dump($arr);
                   //var_dump(sha1($data['password']));
                if(sha1($data['password'])== $arr['geslo'] && $arr['id_statusosebe'] == '4'){
                    
                    //echo ViewHelper::renderJSON($arr['id_oseba']);
                    
                    $stranka['id'] = $arr['id_oseba'];
                    
                    echo ViewHelper::renderJSON($stranka);

                }else if($arr['id_statusosebe'] == '4'){
                    //$cars = array("Volvo", "BMW", "Toyota");
                    $stranka['id'] = 'Wrong password';
                    echo ViewHelper::renderJSON($stranka, 400);
                    //echo ViewHelper::renderJSON($data['password']);
                    //echo ViewHelper::renderJSON(sha1($data['password']));
                    //echo ViewHelper::renderJSON($arr['geslo']);
                    
                }
                else{
                    $stranka['id'] = 'User is not active';
                    echo ViewHelper::renderJSON($stranka, 400);
                    //echo ViewHelper::renderJSON($arr['id_oseba']);
                }
            }
            else {
                $stranka['id'] = 'No such user.';
                echo ViewHelper::renderJSON($stranka, 404);
            }
            
        }
        else{
            $stranka['id'] = 'Part of data is null';
            echo ViewHelper::renderJSON($stranka, 400);
        }
    }
    
    public static function getUserData($id){
        //var_dump($id);
        try {
             $stranka = DB::getSelf($id);
             //var_dump($stranka);
             if($stranka){
                $stranka = $stranka[0];
                $moreData =  DB::getSelfStranka($id);
                $stranka['posta'] = $moreData['posta'];
                $stranka['kraj'] = $moreData['kraj'];
                $stranka['ulica'] = $moreData['ulica'];
                $stranka['stevilka'] = $moreData['stevilka'];
                $stranka['telefonska'] = $moreData['telefonska'];
               echo ViewHelper::renderJSON($stranka);
             }else{
                 echo ViewHelper::renderJSON("No such user.", 400);
             }
            
        } catch (Exception $ex) {
            echo ViewHelper::renderJSON("something went wrong", 500);
        }
    }
    
    public static function changeUserData($id){
        $_PUT = [];
        parse_str(file_get_contents("php://input"), $_PUT);
        
        
        
        $rules = [      
            "priimek" => FILTER_SANITIZE_SPECIAL_CHARS,
            "ime" => FILTER_SANITIZE_SPECIAL_CHARS,
            "username" => FILTER_VALIDATE_EMAIL, //filteriranje emaila.
            "posta" => FILTER_SANITIZE_SPECIAL_CHARS,
            "kraj" => FILTER_SANITIZE_SPECIAL_CHARS,
            "ulica" => FILTER_SANITIZE_SPECIAL_CHARS,
            "stevilka" => FILTER_SANITIZE_SPECIAL_CHARS,
            "telefonska" => FILTER_SANITIZE_SPECIAL_CHARS
        ];
        $data = filter_var_array($_PUT, $rules);
        
        

        
        //$arr['telefon'] = $data['telefonska'];
        
        //var_dump($data);
        //var_dump(strlen($data['address']) <= 49);
        if(self::checkValues($data)){
            if($data['ime']=='' || $data['priimek']=='' || $data["username"] == '' || $data['posta'] == '' || $data['kraj'] == ''
                    || $data['ulica'] == '' || $data['stevilka'] == ''|| $data['telefonska'] == ''){
             echo ViewHelper::renderJSON("input field shoud not be empty..", 400);
            }
            else{
                #echo ViewHelper::renderJSON(strlen((string)$data['telefonska']));
                if(strlen($data['telefonska']) == 9){
                    if((strlen($data['posta']) <= 49 && strlen($data['kraj']) <= 49 && strlen($data['ulica']) && strlen($data['stevilka'])) <= 9){
                        //echo ViewHelper::renderJSON(strlen($data['posta']));
                        $dataOseba = DB::getSelf($id); //OK 
                        if($dataOseba){
                            DB::ChangeDataOseba($id, $data['ime'], $data['priimek'], $dataOseba[0]['id_statusosebe'], $data['username'], $dataOseba[0]['geslo']);
                            DB::ChangeDataStranka($id, $data['telefonska'], $data['posta'], $data['kraj'], $data['ulica'], $data['stevilka'] );
                            $arr['response'] = "ok";
                             echo ViewHelper::renderJSON($arr, 200);
                             //self::getUserData($id);
                        }else{
                            echo ViewHelper::renderJSON("no such user", 400);  
                        }
                    }
                    else{
                       echo ViewHelper::renderJSON("too long data!!", 400);   
                    }
                    
                }else{
                      echo ViewHelper::renderJSON("telephone nuber shoud be 9 digits long", 400); 
                }
                
            }
            
        }else{
            echo ViewHelper::renderJSON("Value-check did not pass. Are you sending all fields?", 400);
            echo ViewHelper::renderJSON("This is what you sent: ");
            echo ViewHelper::renderJSON($_PUT);//spremba nemogoca, ker ni sel cez filter
        } 
    }
    
    public static function changeUserPassword($id){
        $rules = [
            "passwordOld" => FILTER_SANITIZE_SPECIAL_CHARS,
            "passwordNew" => FILTER_SANITIZE_SPECIAL_CHARS
        ];
        $_PUT = [];
        parse_str(file_get_contents("php://input"), $_PUT);
        

        $data = filter_var_array($_PUT, $rules);
        
        //$json = file_get_contents('php://input');
        //$obj = json_decode($json, true);
        
        //echo ViewHelper::renderJSON($obj);
        
        //parse_str(file_get_contents("php://input"), $_PUT);
        //$data = filter_var_array($obj, $rules);
        
        //$entityBody = file_get_contents('php://input');
        //echo ViewHelper::renderJSON("Raw input: ");
        //echo ViewHelper::renderJSON($entityBody);
        //echo ViewHelper::renderJSON("_PUT: ");
        //echo ViewHelper::renderJSON($_PUT);
        
        
        
        //var_dump($data);
        if(isset($data)){
            //var_dump($user);
            $user = DB::getSelf($id)[0];
            //var_dump($user);
            //echo ViewHelper::renderJSON($user['geslo']);
            //echo ViewHelper::renderJSON($data['passwordOld']);
            //echo ViewHelper::renderJSON(sha1($data['passwordOld']));
            
            if($user['geslo'] == sha1($data['passwordOld'])){
                //var_dump("vse vredu, potreben je klic da se geslo spremeni");
                DB::changePassword($id, $data['passwordNew']);
                echo ViewHelper::renderJSON("Pasword successfully changed", 200); 

            }else{
                 echo ViewHelper::renderJSON("Old password incorrect", 400); 
            }
            
            
        }else{
            //var_dump("err");
            //ViewHelper::redirect(BASE_URL . "adminPage");
            echo ViewHelper::renderJSON("Value-check did not pass. Are you sending all fields?", 400);
            echo ViewHelper::renderJSON("This is what you sent: ");
            echo ViewHelper::renderJSON($_PUT);//spremba nemogoca, ker ni sel cez filter
        }
    }
    
    public static function addToCart(){
          $rules = [
            "id" =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ]
        ];
        
        $data = filter_input_array(INPUT_POST, $rules);
        echo ViewHelper::renderJSON("This is what you sent: ");
        echo ViewHelper::renderJSON($data);
        if(self::checkValues($data)){
            Cart::addProductToCart($data['id']);
            
            $arr['response'] = 'item added to cart';
            
            echo ViewHelper::renderJSON($arr, 200);
        }else{
           echo ViewHelper::renderJSON("Value-check did not pass. Are you sending all fields?", 400);
           echo ViewHelper::renderJSON("This is what you sent: ");
           echo ViewHelper::renderJSON($data);//spremba nemogoca, ker ni sel cez filter 
        }
    }
    
    public static function updateInCart(){
        $rules = [
            "id" =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ],
            "amount" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1] //kolicina manjsa od 0 je nekaj hudo narobe...
            ]
        ];
        $_PUT = [];
        parse_str(file_get_contents("php://input"), $_PUT);
        $data = filter_var_array($_PUT, $rules);

        
        //var_dump($data);
         if(self::checkValues($data)){
             Cart::updateProductInCart($data['id'], $data['amount']);
             echo ViewHelper::renderJSON("ok", 200);
        }else{
              echo ViewHelper::renderJSON("something went wrong..", 500); 
        }
    }
    
    public static function deleteInCart($id){
        try {
            Cart::deleteOneProductInCart($id);
            echo ViewHelper::renderJSON("ok", 201);
        } catch (Exception $ex) {
            echo ViewHelper::renderJSON("something went wrong..", 500); 
        }
            
    }
    
    public static function purgeCart(){
        try {
            Cart::purgeCart(); //zbrisi in to je to...preusmeri....
            echo ViewHelper::renderJSON("cart deleted", 201);
        } catch (Exception $ex) {
            echo ViewHelper::renderJSON("something went wrong..", 500);
        }
    }
    
    public static function getCart(){
        try {
           echo ViewHelper::renderJSON(self::getProductINCart(), 200); 
        } catch (Exception $ex) {
            echo ViewHelper::renderJSON("something went wrong..", 500); 
        }
    }
    
    public static function purchase(){
        
        //echo ViewHelper::renderJSON("ok", 200); 
        
        $rules = [
            "id" =>[
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ]
        ];
        
        $data = filter_input_array(INPUT_POST, $rules);
        //echo ViewHelper::renderJSON($data['id']);
        if(self::checkValues($data)){
            try {
                $invoice = self::getProductINCart_INVALID_JSON();
                if($invoice){
                    //echo ViewHelper::renderJSON($invoice);
                    //echo ViewHelper::renderJSON($data);
                    //echo ViewHelper::renderJSON($data['id']);
                    //TODO: Tole meče napako
                    DB::createInvoice(Cart::calculateTotal(), $data['id'], $invoice);
                    //
                    Cart::purgeCart();
                    echo ViewHelper::renderJSON("okey", 200);
                    
                }else{
                    echo ViewHelper::renderJSON("cart is empty", 400); 
                }
                
            } catch (Exception $ex) {
                //echo ViewHelper::renderJSON("Value-check did not pass. Are you sending all fields?", 400);
                echo ViewHelper::renderJSON("This is what you sent: ");
                echo ViewHelper::renderJSON($ex);
                echo ViewHelper::renderJSON($data);
                echo ViewHelper::renderJSON("something went wrong..", 500); 
            }
        }else{
            echo ViewHelper::renderJSON("Value-check did not pass. Are you sending all fields?");
            echo ViewHelper::renderJSON("This is what you sent: ");
            echo ViewHelper::renderJSON($data, 400);//spremba nemogoca, ker ni sel cez filter 
            //echo ViewHelper::renderJSON("something else went totaly wrong..", 500); 
        }
        
    }
    public static function getOsebaAllOrders($idClient){
        try {
            echo ViewHelper::renderJSON(DB::getOsebaNarocila($idClient), 200);
        } catch (Exception $ex) {
              echo ViewHelper::renderJSON("something else went totaly wrong..", 500); 
        }
    }
    
    public static function getOsebaSpecificOrder($invoice_id){
        try {
            echo ViewHelper::renderJSON(DB::getOneInvoice($invoice_id), 200);
            //var_dump(DB::getOneInvoice($invoice_id));
        } catch (Exception $ex) {
            echo ViewHelper::renderJSON("something else went totaly wrong..", 500); 
        }
    }
    
    private static function getProductINCart_INVALID_JSON(){
        return Cart::getProductsInCart();
    }
    
    private static function getProductINCart(){
        //return Cart::getProductsInCart();
        $getItem = Cart::getProductsInCart();
        //$product['quantity'];
        //echo ViewHelper::renderJSON("Tralala, tralali");
        //echo ViewHelper::renderJSON($getItem, 200);
        //$colors = array("red", "green", "blue", "yellow"); 
        
        //$arr = array();
        //$arr[] = 13;
        //$arr[] = 14;
        // etc
        
        /*
        $object = new stdClass();
        $object->name = "My name";
        $myArray[] = $object;
         * 
         */
        
        /*
        foreach($getItem as $key => $value){
            $int = $data[$key]['quantity'];
            $data[$key]['quantity'] = strval($int);
        }
         * 
         */
        /*
        foreach ($getItem as $key => $field) {
            $fields[$key]['quantity'] = strval($field['quantity']);
        }
         * 
         */

        /*
        foreach ($getItem as $value) {
            $int = $value['quantity'];
            $value['quantity'] = strval($int);
            //$arr[];
            //echo ViewHelper::renderJSON("$value");
            echo ViewHelper::renderJSON($value);
        }
         */
        
        foreach($getItem as &$value){
            $value['quantity'] = strval($value['quantity']);
        }

        
        return $getItem;
    }
    
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }
        
        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
}
