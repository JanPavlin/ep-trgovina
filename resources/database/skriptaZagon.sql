/*==============================================================*/
/* dbms name:      mysql 3.22                                   */
/* created on:     27/11/2017 20:16:57                          */
/*==============================================================*/
DROP SCHEMA  IF EXISTS `ep`;
CREATE SCHEMA `ep` ;
use `ep`;


drop table if exists administrator;

drop table if exists akcija;

drop table if exists dnevnik;

drop table if exists izdelek;

drop table if exists narocenizdelek;

drop table if exists narocilo;

drop table if exists ocenaizdelka;

drop table if exists oseba;

drop table if exists prodajalec;

drop table if exists slikaizdelka;

drop table if exists statusizdelka;

drop table if exists statusnarocila;

drop table if exists statusosebe;

drop table if exists stranka;


/*==============================================================*/
/* table: administrator                                         */
/*==============================================================*/
create table administrator
(
   id_oseba                       int                            not null,
   id_administrator				  int							 not null AUTO_INCREMENT,			
   primary key (id_administrator)
);

/*==============================================================*/
/* table: akcija                                               */
/*==============================================================*/
create table akcija
(
   id_akcija                                      int                            not null AUTO_INCREMENT,
   opis_akcija					  char(50)                       not null ,			
   primary key (id_akcija)
);

/*==============================================================*/
/* table: dnevnik                                               */
/*==============================================================*/
create table dnevnik
(
   id_obiska                      int                            not null AUTO_INCREMENT,
   id_oseba                       int                            not null,
   id_akcija					  int		 not null,
   cas_obiska                     datetime                       not null,
   primary key (id_obiska)
);

/*==============================================================*/
/* index: belezenje_obiska_fk                                   */
/*==============================================================*/
create index belezenje_obiska_fk on dnevnik
(
   id_oseba
);
create index belezenje_obiska_fk1 on dnevnik
(
   id_akcija
);

/*==============================================================*/
/* table: izdelek                                               */
/*==============================================================*/
create table izdelek
(
   id_izdelka                     int                            not null AUTO_INCREMENT,
   ime_izdelka					  char(30)						 not null,
   id_statusaizdelka              int                            not null,
   opis_izdelka                   char(240)                      not null,
   postavka                       int                            not null,
   slika_izdelka				  char(50), 
   primary key (id_izdelka)
);

/*==============================================================*/
/* index: status_artikla_fk                                     */
/*==============================================================*/
create index status_artikla_fk on izdelek
(
   id_statusaizdelka
);

/*==============================================================*/
/* table: narocenizdelek                                        */
/*==============================================================*/
create table narocenizdelek
(
   id_izdelka                     int                            not null,
   id_narocilo                    int                            not null,
   kolicina_izdelka               int                            not null
);

/*==============================================================*/
/* index: narocaizdelke_fk                                      */
/*==============================================================*/
create index narocaizdelke_fk on narocenizdelek
(
   id_narocilo
);

/*==============================================================*/
/* index: jenarocen_fk                                          */
/*==============================================================*/
create index jenarocen_fk on narocenizdelek
(
   id_izdelka
);

/*==============================================================*/
/* table: narocilo                                              */
/*==============================================================*/
create table narocilo
(
   id_narocilo                    int                            not null AUTO_INCREMENT,
   id_statusnarocila              int                            not null,
   id_oseba                       int                            not null,
   skupajPlacilo                  int                             not null,
   pro_id_oseba                   int,
   primary key (id_narocilo)
);

/*==============================================================*/
/* index: statusnarocila_fk                                     */
/*==============================================================*/
create index statusnarocila_fk on narocilo
(
   id_statusnarocila
);

/*==============================================================*/
/* index: izdelava_narocila_fk                                  */
/*==============================================================*/
create index izdelava_narocila_fk on narocilo
(
   id_oseba
);

/*==============================================================*/
/* index: potrjevanje_narocila_fk                               */
/*==============================================================*/
create index potrjevanje_narocila_fk on narocilo
(
   pro_id_oseba
);

/*==============================================================*/
/* table: ocenaizdelka                                          */
/*==============================================================*/
create table ocenaizdelka
(
   id_oceneizdelka                int                            not null AUTO_INCREMENT,
   id_izdelka                     int                            not null,
   id_osebe                       int                            not null,
   vrednost                       int                            not null,
   primary key (id_oceneizdelka)
);

/*==============================================================*/
/* index: ocena_artikla_fk                                      */
/*==============================================================*/
create index ocena_artikla_fk on ocenaizdelka
(
   id_izdelka
);

create table slikaizdelka
(
   id_slike                       int                            not null AUTO_INCREMENT,
   id_izdelka                     int                            not null,
   potSlike                       char(40)                            not null,
   primary key (id_slike)
);

/*==============================================================*/
/* index: ocena_artikla_fk                                      */
/*==============================================================*/
create index slikaizdelka_fk on slikaizdelka
(
   id_izdelka
);

/*==============================================================*/
/* table: oseba                                                 */
/*==============================================================*/
create table oseba
(
   id_oseba                       int                            not null AUTO_INCREMENT,
   id_statusosebe                 int                            not null,
   priimek                        char(30)                       not null,
   ime                            char(30)                       not null,
   geslo                          char(40)                       not null,
   username                       char(30)                       not null,
   primary key (id_oseba)
);

/*==============================================================*/
/* index: statusosebe_fk                                        */
/*==============================================================*/
create index statusosebe_fk on oseba
(
   id_statusosebe
);

/*==============================================================*/
/* table: prodajalec                                            */
/*==============================================================*/
create table prodajalec
(
   id_oseba                       int                            not null,
   id_prodajalec				  int							 not null AUTO_INCREMENT,
   id_adm		                  int                            not null,
   
   primary key (id_prodajalec)
);

/*==============================================================*/
/* index: ustvaril_fk                                           */
/*==============================================================*/
create index ustvaril_fk on prodajalec
(
   id_adm
);


/*==============================================================*/
/* table: statusizdelka                                         */
/*==============================================================*/
create table statusizdelka
(
   id_statusaizdelka              int                            not null,
   opis_statusaizdelka            char(10),
   primary key (id_statusaizdelka)
);

/*==============================================================*/
/* table: statusnarocila                                        */
/*==============================================================*/
create table statusnarocila
(
   id_statusnarocila              int                            not null,
   opis_statusnarocila            char(30)                       not null,
   primary key (id_statusnarocila)
);

/*==============================================================*/
/* table: statusosebe                                           */
/*==============================================================*/
create table statusosebe
(
   id_statusosebe                 int                            not null,
   opis_statusosebe               char(30)                       not null,
   primary key (id_statusosebe)
);

/*==============================================================*/
/* table: stranka                                               */
/*==============================================================*/
create table stranka
(
   id_oseba                       int                            not null,
   id_stranka 					  int 							 not null AUTO_INCREMENT, 
   posta						  int							 not null, 
   kraj							  char(50)						 not null,
   ulica						  char(50)						 not null,
   stevilka						  char(10)                       not null,
   telefonska                     char(10)                       not null,
   primary key (id_stranka)
);

/*==============================================================*/
/* Ddodajanje vrednosti                                         */
/*==============================================================*/

/*==============================================================*/
/* STATUSI                                                      */
/*==============================================================*/

INSERT INTO `ep`.`statusizdelka`
(`ID_STATUSAIZDELKA`,
`OPIS_STATUSAIZDELKA`)
VALUES
(1,"aktiven");
INSERT INTO `ep`.`statusizdelka`
(`ID_STATUSAIZDELKA`,
`OPIS_STATUSAIZDELKA`)
VALUES
(2,"neaktiven");

INSERT INTO `ep`.`statusnarocila`
(`ID_STATUSNAROCILA`,
`OPIS_STATUSNAROCILA`)
VALUES
(1,"nepotrjeno");
INSERT INTO `ep`.`statusnarocila`
(`ID_STATUSNAROCILA`,
`OPIS_STATUSNAROCILA`)
VALUES
(2,"potrjeno");
INSERT INTO `ep`.`statusnarocila`
(`ID_STATUSNAROCILA`,
`OPIS_STATUSNAROCILA`)
VALUES
(3,"stornirano");


INSERT INTO `ep`.`statusosebe`
(`ID_STATUSOSEBE`,
`OPIS_STATUSOSEBE`)
VALUES
(1,"administrator");
INSERT INTO `ep`.`statusosebe`
(`ID_STATUSOSEBE`,
`OPIS_STATUSOSEBE`)
VALUES
(2,"prodajalec aktiven");
INSERT INTO `ep`.`statusosebe`
(`ID_STATUSOSEBE`,
`OPIS_STATUSOSEBE`)
VALUES
(3,"prodajalec neaktiven");
INSERT INTO `ep`.`statusosebe`
(`ID_STATUSOSEBE`,
`OPIS_STATUSOSEBE`)
VALUES
(4,"uporabnik aktiven");
INSERT INTO `ep`.`statusosebe`
(`ID_STATUSOSEBE`,
`OPIS_STATUSOSEBE`)
VALUES
(5,"neaktiven");

/*==============================================================*/
/* Uporabniki sistema                                           */
/*==============================================================*/ 

INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(1,1,"Norris", "Chuck", "881baaede9ab674cbb97cddae8bfda41d8ad51c3" , "ep@gmail.com" );

INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(2,2,"Zoretic", "Uros", "2401bc1003df5c832d17391d0035b6b98ec15e37", "uros@gmail.com");

INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(3,2,"Pavlin", "Jan","14e793d896ddc8ca6911747228e86464cf420065","jan@gmail.com");

INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(4,2,"Karpljuk", "Gregor","5fdc67d2166bcdd1d3aa4ed45ea5a25e9b21bc20", "gregor@gmail.com");


INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(5,4,"Novak", "Janez",  "bd629304ecef22fd2a0a921cbb94ade0d814d4ea", "epstranka@gmail.com");

INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(7,4,"geslo je qwertz", "Janez",  "8c829ee6a1ac6ffdbcf8bc0ad72b73795fff34e8", "janez@novak.si");

INSERT INTO `ep`.`stranka`
(`id_oseba`,
`id_stranka`,
`posta`,
`kraj`,
`ulica`,
`stevilka`,
`telefonska`)
VALUES
(7,1, 8000, "Novo mesto", "Kandija","69",030030030);

INSERT INTO `ep`.`administrator`
(`id_oseba`)
VALUES
(1);

INSERT INTO `ep`.`prodajalec`
(`id_oseba`,
 `id_prodajalec`,
`id_adm`)
VALUES
(2,1,1);


INSERT INTO `ep`.`izdelek`
(`ID_IZDELKA`,
`ID_STATUSAIZDELKA`,
`IME_IZDELKA`,
`OPIS_IZDELKA`,
`POSTAVKA`,
`SLIKA_IZDELKA`)
VALUES
(1,1,"lesena palica","to je palica, ki je narejena iz lesa", 0.66, "300px-Wooden_Stick");

INSERT INTO `ep`.`izdelek`
(`ID_IZDELKA`,
`ID_STATUSAIZDELKA`,
`IME_IZDELKA`,
`OPIS_IZDELKA`,
`POSTAVKA`,
`SLIKA_IZDELKA`)
VALUES
(2,1,"kovinska palica","to je palica, ki je narejena iz kovine", 2.66, "300px-Wooden_Stick");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(1,"registracija prodajalca");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(2,"change data stranka");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(3,"delete oseba");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(4,"add izdelek");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(5,"delete izdelek");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(6,"change status izdelka");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(7,"create narocilo");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(8,"change status narocilo");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(9,"add ocena");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(10,"insert slika");

