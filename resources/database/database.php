<?php

require_once 'database_init.php';

class DB {
    #registracija example-->  #DB::register(3, "jože", "gorišek", "joze","Boze");
     public static function registerStranka($ime, $priimek,
             $username,$geslo, $telefon, $posta, $kraj, $ulica, $stevilka){
        $db = DBInit::getInstance();
         //najprej preveri, ce je oseba ze v uporabi, torej ce je ze registrirana...
        
        $statement = $db->prepare("SELECT * FROM `oseba`"
                ." WHERE `username` = :username " );
   
        $statement->bindParam(":username", $username);
        
        $statement->execute();
        $arr = $statement->fetch();
        if(empty($arr)){
            $bla = 5;
            $g = sha1($geslo);

           $statement = $db->prepare("insert into oseba "
                   ."(`id_statusosebe`,`priimek`,"
                   ."`ime`,`geslo`,`username`) VALUES"
                   ."(:id, :priimek, :ime, :geslo, :username)");
           $statement->bindParam(":username", $username);
           $statement->bindParam(":ime", $ime);
           $statement->bindParam(":geslo", $g);
           $statement->bindParam(":id", $bla);
           $statement->bindParam(":priimek",$priimek);
           $statement->execute();
           $val = ($db->lastInsertId());
           $statement = $db->prepare("insert into stranka "
                   ."(`id_oseba`,`posta`,`kraj`,`ulica`,`stevilka`,"
                   ."`telefonska`) VALUES"
                   ."(:id, :tel, :pos, :kraj, :uli, :st)");
           $statement->bindParam(":id", $val);
           $statement->bindParam(":tel", $telefon);
           $statement->bindParam(":pos", $posta);
           $statement->bindParam(":kraj", $kraj);
           $statement->bindParam(":uli", $ulica);
           $statement->bindParam(":st", $stevilka);
        
           $statement->execute();
           
           $actual_link;
           
           var_dump("$_SERVER[REQUEST_URI]");
           if("$_SERVER[REQUEST_URI]" == '/netbeans/ep-trgovina/mainPages/seller/seller.php/newClient?&error=0'){
               $actual_link = "http://$_SERVER[HTTP_HOST]".rtrim("$_SERVER[REQUEST_URI]", "seller.php/newClient?&error=0")."es/client/client.php/regisactivate?id=" . $val;
           }else{
               $actual_link = "http://$_SERVER[HTTP_HOST]".rtrim("$_SERVER[REQUEST_URI]", "newClient?&error=0")."activate?id=" . $val;
           }
            var_dump($actual_link);
           
                $mail = new PHPMailer(false); //defaults to using php "mail()"; the true param means it will throw exceptions on errors, which we need to catch
           
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
                //$mail->SMTPDebug = 4;                               // Enable verbose debug output
                $mail->isSMTP();                                    // Set mailer to use SMTP
                $mail->Host = 'smtp.gmail.com';                     // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                             // Enable SMTP authentication
                $mail->Username = 'ep2017.trgovina@gmail.com';           // SMTP username
                $mail->Password = 'ep2017janurosgregor';                       // SMTP password
                $mail->SMTPSecure = 'tls';                          // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                  // TCP port to connect, tls=587, ssl=465
                $mail->From = 'ep2017.trgovina@gmail.com';
                $mail->FromName = 'noreply';
                $mail->addAddress($username);     // Add a recipient --- za testirat bo kr lasten e-mail ---> TODO ZA KONČNO ODDAJO SE PRILAGODI...
                //$mail->addReplyTo($fields['email'], $fields['name']);
                $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = 'Accout confirmation';
                $mail->Body    = "Click this link to activate your account. <a href='" . $actual_link . "'>" . $actual_link . "</a>";
                $mail->AltBody = 'Sorry email client shoud support HTML';
                if(!$mail->send()) {
                    echo 'Message could not be sent.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                    return false;
                } else {
                    echo 'Message has been sent';
                    return true;
                }
        
           
        }else{
            //return false;
            var_dump(false);
        }
         
     
       
        //trigger za vpis, da se shrani vrednost v dnevnik
        
    }
    public static function registerProdajalec($ime, $priimek,
             $username,$geslo, $status ,$id_adm){
         $bla = $status;
         $geslo = sha1($geslo);
         #var_dump($id_statusizdelka, $ime,
        #     $priimek,$username,$geslo);
        $db = DBInit::getInstance();
        
        $statement = $db->prepare("insert into oseba "
                ."(`id_statusosebe`,`priimek`,"
                ."`ime`,`geslo`,`username`) VALUES"
                ."(:id, :priimek, :ime, :geslo, :username)");
        $statement->bindParam(":username", $username);
        $statement->bindParam(":ime", $ime);
        $statement->bindParam(":geslo",$geslo);
        $statement->bindParam(":id", $bla);
        $statement->bindParam(":priimek",$priimek);
        $statement->execute();
        $val = ($db->lastInsertId());
        $statement = $db->prepare("insert into prodajalec "
                ."(`id_oseba`,`id_adm`) VALUES"
                ."(:id, :ida)");
        $statement->bindParam(":id", $val);
        $statement->bindParam(":ida", $id_adm);
        $statement->execute();
        var_dump($_SESSION["admin"]);
        self::addLog($_SESSION["admin"],'1');
       
        //trigger za vpis, da se shrani vrednost v dnevnik
        
    }
    public static function ChangeDataOseba($id, $ime, $priimek,$id_statusosebe,
             $username,$geslo){
        
         #var_dump($id_statusizdelka, $ime,
        #     $priimek,$username,$geslo);
        //var_dump("klicem");
        $db = DBInit::getInstance();
        $statement = $db->prepare("UPDATE `oseba` SET "
                ."`id_oseba` = :id, `id_statusosebe` = :ids,"
                ." `priimek` = :priimek, `ime` = :ime, "
                ." `geslo` = :geslo, `username` = :username "
                ." WHERE `id_oseba` = :id " );
        $statement->bindParam(":username", $username);
        $statement->bindParam(":ids", $id_statusosebe);
        $statement->bindParam(":ime", $ime);
        $statement->bindParam(":geslo",$geslo);
        $statement->bindParam(":id", $id);
        $statement->bindParam(":priimek",$priimek);
        $statement->execute();
        if (isset($_SESSION["seller"]) && !empty($_SESSION["seller"])){
            $val = $_SESSION["seller"];
        } 
        if (isset($_SESSION["admin"]) && !empty($_SESSION["admin"])){
             $val = $_SESSION["admin"];
        } 
        if (isset($_SESSION["client"]) && !empty($_SESSION["client"])){
             $val = $_SESSION["client"];
        } else{
            $val = $id;
        }
        
        self::addLog($val,'2');
        //trigger za vpis, da se shrani vrednost v dnevnik    
    }
    
    public static function changeOsebaStatus($id, $statusOsebe){
        $db = DBInit::getInstance();
        $statement = $db->prepare("UPDATE `oseba` SET "
                ."`id_statusosebe` = :ids "
                ." WHERE `id_oseba` = :id " );
   
        $statement->bindParam(":ids", $statusOsebe);
        $statement->bindParam(":id", $id);
        $statement->execute();
    }
    
    public static function ChangeDataStranka($id, $telefon, $posta, $kraj, $ulica, $stevilka){
      
        $db = DBInit::getInstance();
        $statement = $db->prepare("UPDATE `ep`.`stranka` SET "
                ."`id_oseba` = :id, `posta` = :pos, `kraj` = :kraj,"
                ." `ulica` = :uli,`stevilka` = :st, `telefonska` = :telefon "
                ." WHERE `id_oseba` = :id ;" );

        $statement->bindParam(":telefon", $telefon);
        $statement->bindParam(":id", $id);
        $statement->bindParam(":pos", $posta);
        $statement->bindParam(":kraj", $kraj);
        $statement->bindParam(":uli", $ulica);
        $statement->bindParam(":st", $stevilka);
        $statement->execute();
        //trigger za vpis, da se shrani vrednost v dnevnik     
    }
    
    public static function changePassword($id, $pass) {
        $newPass = sha1($pass);
        $db = DBInit::getInstance();
        $statement = $db->prepare("UPDATE `ep`.`oseba` SET"
                ."`geslo` = :geslo WHERE id_oseba = :id;)" );
        $statement->bindParam(":id", $id);
        $statement->bindParam(":geslo", $newPass);
        
        //trigger za vpis, da se shrani vrednost v dnevnik
        $statement->execute();
    }
    
    public static function login($username) {
        
        $db = DBInit::getInstance();
        $statement = $db->prepare("SELECT * FROM oseba"
                . " WHERE username = :username");
        $statement->bindParam(":username", $username);
        //trigger za vpis, da se shrani vrednost v dnevnik
        $statement->execute();

        return $statement->fetch();
    }
    
    #brisnje uporabnika na id
     public static function deleteOsebaId($id) {
        $val;
        if (isset($_SESSION["seller"]) && !empty($_SESSION["seller"])){
            $val = $_SESSION["seller"];
        } 
        if (isset($_SESSION["admin"]) && !empty($_SESSION["admin"])){
             $val = $_SESSION["admin"];
        } 
         if (isset($_SESSION["client"]) && !empty($_SESSION["client"])){
             $val = $_SESSION["client"];
        } 
        self::addLog($val,'3');
        
        $db = DBInit::getInstance();

        $statement = $db->prepare("DELETE FROM oseba WHERE id_oseba = :id_oseba");
        $statement->bindParam(":id_oseba", $id, PDO::PARAM_INT);
        $statement->execute();
      
    }
    #brisnje uporabnika na ime--> NOTE -> vsa enaka uporabniska imena se
     public static function deleteOsebaUsername($username) {
        $db = DBInit::getInstance();
        var_dump($username);
        $statement = $db->prepare("DELETE FROM oseba WHERE username = :username");
        $statement->bindParam(":username", $username, PDO::PARAM_INT);
        $statement->execute();
    }
    
    public static function getOsebaAll() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("SELECT id_oseba, id_statusosebe, priimek, ime,"
                . " , username FROM oseba ");
        $statement->execute();
        return $statement->fetchAll();
    }
    #pridobitev podatkov za vse osebe
        public static function getSelf($id) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("SELECT id_oseba, id_statusosebe, priimek, ime,"
                . "geslo, username FROM oseba WHERE id_oseba = :id_oseba");
        $statement->bindParam(":id_oseba", $id, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll();
    }
    #pridobitev dodatnih podatkov za prodajalca
        public static function getSelfProdajalec($id) {
        $db = DBInit::getInstance();
        
        $statement = $db->prepare("SELECT id_adm "
            . "FROM prodajalec "
            . "WHERE id_oseba = :id_oseba ");
            $statement->bindParam(":id_oseba", $id, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetch();
    }
    
        public static function getAllProdajalec() {
        $db = DBInit::getInstance();
       
        $statement = $db->prepare("SELECT id_oseba, id_statusosebe, priimek, ime,"
                . "geslo, username FROM oseba "
                . "WHERE id_statusosebe = 2 OR  id_statusosebe = 3 ");
        $statement->execute();
            
        return $statement->fetchAll(); 
        }    
        public static function getActiveProdajalec() {
        $db = DBInit::getInstance();
       
        $statement = $db->prepare("SELECT id_oseba, id_statusosebe, priimek, ime,"
                . "geslo, username FROM oseba "
                . "WHERE id_statusosebe = 2  ");
        $statement->execute();
            
        return $statement->fetchAll(); 
        }   
        
        public static function getInactiveProdajalec() {
        $db = DBInit::getInstance();
       
        $statement = $db->prepare("SELECT id_oseba, id_statusosebe, priimek, ime,"
                . "geslo, username FROM oseba "
                . "WHERE id_statusosebe = 2  ");
        $statement->execute();
            
        return $statement->fetchAll(); 
        }    
        
        public static function getAllStranka() {
        $db = DBInit::getInstance();
        $db = DBInit::getInstance();
        $statement = $db->prepare("SELECT id_oseba, id_statusosebe, priimek, ime,"
                . "geslo, username FROM oseba "
                . "WHERE id_statusosebe = 4 OR  id_statusosebe = 5 ");
        $statement->execute();
            
        return $statement->fetchAll();
    
    }
    #pridobitev dodatnih podatkov za stranko
        public static function getSelfStranka($id) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("select * "
            . "from stranka  "
            . "where id_oseba = :id_oseba" );
        $statement->bindParam(":id_oseba", $id, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetch();
    }
    #pridobitev podatkov za aktivne izdelke
    public static function getIzdelekActive() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("select * "
                ."from izdelek where id_statusaizdelka = 1");
        $statement->execute();
        return $statement->fetchAll();
    }
    //pridobi en izdelek na podlagi id-ja
    public static  function getOneProduct($id){
        $db = DBInit::getInstance();
        $statement = $db->prepare("select * "
            . "from izdelek  "
            . "where ID_IZDELKA = :id_izdelka" );
        $statement->bindParam(":id_izdelka", $id, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetch();
    }
    #pridobitev podatkov za vse izdelke
    public static function getIzdelekAll() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("select * "
                ."from izdelek");
        $statement->execute();
        return $statement->fetchAll();
    }
    
    public static function addIzdelek($ime_izdelka, $id_statusizdelka,
            $opis_izdelka, $postavka) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("INSERT INTO izdelek "
                ."(`ime_izdelka` , `id_statusaizdelka` , `opis_izdelka`, `postavka`) "
                ."VALUES ( :ime_izdelka, :id_statusaizdelka, :opis_izdelka,"
                .":postavka)"); 
        
        $statement->bindParam(":ime_izdelka", $ime_izdelka);
        $statement->bindParam(":id_statusaizdelka", $id_statusizdelka, PDO::PARAM_INT);
        $statement->bindParam(":opis_izdelka", $opis_izdelka);
        $statement->bindParam(":postavka", $postavka, PDO::PARAM_INT);
        
        $statement->execute();
        self::addLog($_SESSION["seller"],'4');
        
    }
    
    public static function deleteIzdelek($id) {
        $db = DBInit::getInstance();
        
        $statement = $db->prepare("DELETE FROM izdelek WHERE id_izdelka = :id");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        self::addLog($_SESSION["seller"],'5');
    }
    
    public static function changeDataIzdelek($id, $imeIzdelka,$opisIzdelka, $cena){
        $db = DBInit::getInstance();
        $statement = $db->prepare("UPDATE `ep`.`izdelek` SET "
                ." `ime_izdelka` = :imeIzdelka,"
                ." `opis_izdelka` = :opisIzdelka, `postavka` = :cena"
                ." WHERE `id_izdelka` = :id ;" );
        
        $statement->bindParam(":imeIzdelka", $imeIzdelka);
        $statement->bindParam(":opisIzdelka", $opisIzdelka);
        $statement->bindParam(":cena", $cena);
        $statement->bindParam(":id", $id);
        $statement->execute();
        self::addLog($_SESSION["seller"],'6');
    }
    public static function chnageStatusIzdelka($id, $status){
        $db = DBInit::getInstance();
        $statement = $db->prepare("UPDATE `ep`.`izdelek` SET "
                ." `id_statusaizdelka` = :status"
                ." WHERE `id_izdelka` = :id ;" );
        
        $statement->bindParam(":status", $status);
        $statement->bindParam(":id", $id);
        $statement->execute();
        self::addLog($_SESSION["seller"],'6');
    }
    
    public static function getAllProductInBasket($ids){
        $db = DBInit::getInstance();

        $id_placeholders = implode(",", array_fill(0, count($ids), "?"));
        $statement = $db->prepare("SELECT * FROM izdelek 
            WHERE ID_IZDELKA IN (" . $id_placeholders . ")");
        $statement->execute($ids);

        return $statement->fetchAll();
    }
    
    private static function dodajIzdelekVRacun($id_izdelka, $id_narocilo, $kolicina, $db){
         
        $statement = $db->prepare("insert into narocenizdelek "
                ."(`id_izdelka`,`id_narocilo`,"
                ."`kolicina_izdelka`) VALUES"
                ."(:id_izdelka, :id_narocila, :kol)");
        $statement->bindParam(":id_izdelka", $id_izdelka);
        $statement->bindParam(":id_narocila", $id_narocilo);
        $statement->bindParam(":kol", $kolicina);
        $statement->execute();
    }
    ##############KREIRANJE NAROCILA IN VSTAVLJANJE V NJEGA
       public static function createInvoice($total, $oseba_id, $vsaNarocila){
         $status = 1; //torej nepotrjeno sporocilo, kreirano na novo bo status...
         
        $db = DBInit::getInstance();
        $statement = $db->prepare("insert into narocilo "
                ."(`id_statusnarocila`,`id_oseba`,"
                ."`skupajPlacilo`) VALUES"
                ."(:id_statusnarocila, :id_oseba, :skupajPlacilo)");
        $statement->bindParam(":id_statusnarocila", $status);
        $statement->bindParam(":id_oseba", $oseba_id);
        $statement->bindParam(":skupajPlacilo", $total);
       
        $statement->execute();
        $val = ($db->lastInsertId());
        
        //var_dump($val);
        //var_dump($vsaNarocila);
        
        foreach($vsaNarocila as $key => $izdelek){
            self::dodajIzdelekVRacun($izdelek['id_izdelka'],$val,$izdelek['quantity'], $db);
        }
        
        if(isset($_SESSION["client"]) && !empty($_SESSION["client"])){
            self::addLog($_SESSION["client"],'7');
        }else{
            self::addLog($oseba_id,'7');
        }
        
        //trigger za vpis, da se shrani vrednost v dnevnik
        
    }
    
    public static function getOsebaNarocila($id_oseba){
        $db = DBInit::getInstance();
        $statement = $db->prepare("select * from narocilo "
                . "where id_oseba = :id");
           $statement->bindParam(":id", $id_oseba);
        $statement->execute();
        return $statement->fetchAll();
    }
    
    public static function getOneInvoice($id_narocila){
         $db = DBInit::getInstance();
        $statement = $db->prepare("select * from izdelek INNER JOIN narocenizdelek ON izdelek.id_izdelka = narocenizdelek.id_izdelka ". "where narocenizdelek.id_narocilo = :id_narocila");
           $statement->bindParam(":id_narocila", $id_narocila);
        $statement->execute();
        return $statement->fetchAll();
    }
    
    public static function getInvoice($id){
        $db = DBInit::getInstance();
        $statement = $db->prepare("select * from narocilo "
                . "where id_narocilo = :id");
         $statement->bindParam(":id", $id);
        $statement->execute();
        return $statement->fetch();
    }
    
    public static function changeInvoiceStatus($id_narocila, $status){
         $db = DBInit::getInstance();
        $statement = $db->prepare("UPDATE `ep`.`narocilo` SET "
                ." `id_statusnarocila` = :status"
                ." WHERE `id_narocilo` = :id_narocila ;" );
        
        $statement->bindParam(":status", $status);
        $statement->bindParam(":id_narocila", $id_narocila);
        $statement->execute();
        self::addLog($_SESSION["seller"],'8');
    }
    
    public static function getNarocilaPotrjena(){
        $db = DBInit::getInstance();
        $statement = $db->prepare("select * from narocilo "
                . "where id_statusnarocila = 2");
        $statement->execute();
        return $statement->fetchAll();
    }
    
    public static function getNarocilaStornirana(){
        $db = DBInit::getInstance();
        $statement = $db->prepare("select * from narocilo "
                . "where id_statusnarocila = 3");
        $statement->execute();
        return $statement->fetchAll();
    }

    #pregled neobdelanih naročil
    public static function getNarocilaNeobdelana() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("select * from narocilo "
                . "where id_statusnarocila = 1");
        $statement->execute();
        return $statement->fetchAll();
    }
    #pregled vseh naročil
    public static function getNarocilaAll() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("select * from narocilo");
        $statement->execute();
        return $statement->fetchAll();
    }
    
    public static function insertSlika( $id_izdelka, $potSlike) {
        $db = DBInit::getInstance();
        
        $statement = $db->prepare("SELECT * FROM `slikaizdelka`"
                ." WHERE `potSlike` = :pot AND `id_izdelka` =:izdelek" );
   
        $statement->bindParam(":pot", $potSlike);
        $statement->bindParam(":izdelek", $id_izdelka);
        
        $statement->execute();
        $arr = $statement->fetch();
        if(empty($arr)){

            $statement = $db->prepare("INSERT INTO `ep`.`slikaizdelka`"
            . "(`id_izdelka`, `potSlike`) VALUES ( :izdelek, :slika) ");
            $statement->bindParam(":slika", $potSlike);
            $statement->bindParam(":izdelek", $id_izdelka);

            $statement->execute();
            
            return true;
        }else{
            return false;
        }
        
        
    }
    
    public static function getSlika($id_izdelka){
         $db = DBInit::getInstance();
        $statement = $db->prepare("select slikaizdelka.potSlike, slikaizdelka.id_slike from slikaizdelka INNER JOIN izdelek ON izdelek.id_izdelka = slikaizdelka.id_izdelka ". "where izdelek.id_izdelka = :id");
           $statement->bindParam(":id", $id_izdelka);
        $statement->execute();
        return $statement->fetchAll();
    }
    
    public static function deleteProductSlika($id_izdelka, $id_slike){
        $db = DBInit::getInstance();
        
        $statement = $db->prepare("DELETE FROM slikaizdelka WHERE id_izdelka = :idZ AND id_slike = :idS");
        $statement->bindParam(":idZ", $id_izdelka, PDO::PARAM_INT);
        $statement->bindParam(":idS", $id_slike, PDO::PARAM_INT);
        $statement->execute();
    }
    
     public static function addOcena($id, $ocena, $id_osebe) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("INSERT INTO ocenaizdelka "
                ."(`id_izdelka`, `id_osebe`, `vrednost`) "
                ."VALUES ( :id_izdelka , :id_osebe ,:vrednost)"); 
        
        $statement->bindParam(":id_izdelka", $id);
        $statement->bindParam(":vrednost", $ocena, PDO::PARAM_INT);
        $statement->bindParam(":id_osebe", $id_osebe);
        $statement->execute();
        self::addLog($_SESSION["client"],'8');
        
    }
    public static function allOcenaZaProduct($id_product){
        $db = DBInit::getInstance();
        $statement = $db->prepare("select ocenaizdelka.vrednost, ocenaizdelka.id_osebe from ocenaizdelka INNER JOIN izdelek ON izdelek.id_izdelka = ocenaizdelka.id_izdelka ". "where izdelek.id_izdelka = :id");
           $statement->bindParam(":id", $id_product);
        $statement->execute();
        return $statement->fetchAll();
    }
    public static function addLog($id, $action){
        $date = date("Y-m-d H:i:s");
        $db = DBInit::getInstance();
        $statement = $db->prepare("INSERT INTO dnevnik "
                ."(`id_oseba`, `id_akcija`, `cas_obiska`) "
                ."VALUES ( :id , :action ,:date)");
           $statement->bindParam(":id", $id);
           $statement->bindParam(":action", $action);
           $statement->bindParam(":date", $date);
        $statement->execute();
    }
    public static function getLog(){
        $db = DBInit::getInstance();
        $statement = $db->prepare("(select d.cas_obiska, d.id_obiska, o.username, a.opis_akcija"
                ." from akcija a, dnevnik d, oseba o "
                ." where o.id_oseba = d.id_oseba and d.id_akcija = a.id_akcija)");
        $statement->execute();
        return $statement->fetchAll();
    }
   

}

 #$help = DBJokes::getSelfProdajalec(2)['id_adm'];
            #DBJokes::deleteOsebaUsername("joze");
            #DBJokes::deleteOsebaId(4);
            #DBJokes::register(3, "jože", "gorišek", "joze","Boze");
            #DBJokes::addIzdelek('lool',2,'nekej novga', 5);
            #DBJokes::deleteIzdelek(19);