/*==============================================================*/
/* Ddodajanje vrednosti                                         */
/*==============================================================*/

/*==============================================================*/
/* STATUSI                                                      */
/*==============================================================*/

INSERT INTO `ep`.`statusizdelka`
(`ID_STATUSAIZDELKA`,
`OPIS_STATUSAIZDELKA`)
VALUES
(1,"aktiven");
INSERT INTO `ep`.`statusizdelka`
(`ID_STATUSAIZDELKA`,
`OPIS_STATUSAIZDELKA`)
VALUES
(2,"neaktiven");

INSERT INTO `ep`.`statusnarocila`
(`ID_STATUSNAROCILA`,
`OPIS_STATUSNAROCILA`)
VALUES
(1,"nepotrjeno");
INSERT INTO `ep`.`statusnarocila`
(`ID_STATUSNAROCILA`,
`OPIS_STATUSNAROCILA`)
VALUES
(2,"potrjeno");
INSERT INTO `ep`.`statusnarocila`
(`ID_STATUSNAROCILA`,
`OPIS_STATUSNAROCILA`)
VALUES
(3,"stornirano");


INSERT INTO `ep`.`statusosebe`
(`ID_STATUSOSEBE`,
`OPIS_STATUSOSEBE`)
VALUES
(1,"administrator");
INSERT INTO `ep`.`statusosebe`
(`ID_STATUSOSEBE`,
`OPIS_STATUSOSEBE`)
VALUES
(2,"prodajalec aktiven");
INSERT INTO `ep`.`statusosebe`
(`ID_STATUSOSEBE`,
`OPIS_STATUSOSEBE`)
VALUES
(3,"prodajalec neaktiven");
INSERT INTO `ep`.`statusosebe`
(`ID_STATUSOSEBE`,
`OPIS_STATUSOSEBE`)
VALUES
(4,"uporabnik aktiven");
INSERT INTO `ep`.`statusosebe`
(`ID_STATUSOSEBE`,
`OPIS_STATUSOSEBE`)
VALUES
(5,"neaktiven");

/*==============================================================*/
/* Uporabniki sistema                                           */
/*==============================================================*/ 

INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(1,1,"Norris", "Chuck", "881baaede9ab674cbb97cddae8bfda41d8ad51c3" , "ep@gmail.com" );

INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(2,2,"Zoretic", "Uros", "2401bc1003df5c832d17391d0035b6b98ec15e37", "uros@gmail.com");

INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(3,2,"Pavlin", "Jan","14e793d896ddc8ca6911747228e86464cf420065","jan@gmail.com");

INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(4,2,"Karpljuk", "Gregor","5fdc67d2166bcdd1d3aa4ed45ea5a25e9b21bc20", "gregor@gmail.com");


INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(5,4,"Novak", "Janez",  "bd629304ecef22fd2a0a921cbb94ade0d814d4ea", "epstranka@gmail.com");

INSERT INTO `ep`.`oseba`
(`id_oseba`,
`id_statusosebe`,
`priimek`,
`ime`,
`geslo`,
`username`)
VALUES
(7,4,"geslo je qwertz", "Janez",  "8c829ee6a1ac6ffdbcf8bc0ad72b73795fff34e8", "janez@novak.si");

INSERT INTO `ep`.`stranka`
(`id_oseba`,
`id_stranka`,
`posta`,
`kraj`,
`ulica`,
`stevilka`,
`telefonska`)
VALUES
(7,1, 8000, "Novo mesto", "Kandija","69",030030030);

INSERT INTO `ep`.`administrator`
(`id_oseba`)
VALUES
(1);

INSERT INTO `ep`.`prodajalec`
(`id_oseba`,
 `id_prodajalec`,
`id_adm`)
VALUES
(2,1,1);


INSERT INTO `ep`.`izdelek`
(`ID_IZDELKA`,
`ID_STATUSAIZDELKA`,
`IME_IZDELKA`,
`OPIS_IZDELKA`,
`POSTAVKA`,
`SLIKA_IZDELKA`)
VALUES
(1,1,"lesena palica","to je palica, ki je narejena iz lesa", 0.66, "300px-Wooden_Stick");

INSERT INTO `ep`.`izdelek`
(`ID_IZDELKA`,
`ID_STATUSAIZDELKA`,
`IME_IZDELKA`,
`OPIS_IZDELKA`,
`POSTAVKA`,
`SLIKA_IZDELKA`)
VALUES
(2,1,"kovinska palica","to je palica, ki je narejena iz kovine", 2.66, "300px-Wooden_Stick");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(1,"registracija prodajalca");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(2,"change data stranka");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(3,"delete oseba");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(4,"add izdelek");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(5,"delete izdelek");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(6,"change status izdelka");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(7,"create narocilo");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(8,"change status narocilo");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(9,"add ocena");

INSERT INTO `ep`.`akcija`
(`id_akcija`,
`opis_akcija`)
VALUES
(10,"insert slika");